# Anomaly Vehicle Plugin
Since SUMO only supports normal traffic simulation, we will need to customize our own plugin to add anomalies in our simulation. To do that we need to make use of Traffic Control Interface (TraCI) from SUMO. 

Useful links:

  - http://sumo.dlr.de/wiki/TraCI/Change_Vehicle_State
  - www.sumo.dlr.de/daily/pydoc/
  - Definitions in /sumo/src/utils/traci/TraCIAPI.h
  - Definitions in /sumo/src/libsumo/Vehicle.h

TraCI is accessible natively with SUMO, but the interface and documentation are not very well maintained. It is originaly written in c++, but the documentation is for python, and the documentations lack of description.

The plugin is located in /plugin. The SUMO config files are in /input. The folder /output contains simulation results of several different runs.

You can either download the OSM file from their website or run the script `genOSM.sh` (if the database is online). Run `genOSM.sh --help` for options.

If you download from OSM website, you need to put the OSM file in /input and run python script `convert.py` in /scripts. If you use `genOSM.sh` then no further action is required.

Then run SUMO using `runSUMO.sh` or `sumo-gui -c final.sumo.cfg`. The later does not have anomaly vehicles. To have anomaly vehicles in simulation, you need to setup server-client (see below). If you use `runSUMO.sh` then it automatically setup as anomaly vehicles. The settings for anomaly vehicles are in /plugin/settings.

## Install SUMO
Where are using version 0.32.0 (there is already newer version, but the plugins is not tested for newer version)

To simplify things, you can run `setup_linux_env.sh` from Pattern of Life, then run `setup_sumo_env.sh`

If you don't have access to `setup_linux_env.sh`, you can still run `setup_sumo_env.sh`, but there might be missing libraries you need to install manually

To install SUMO manually without `setup_sumo_env.sh`

Install OpenSceneGraph (OSG)
```
cd ~/Downloads
git clone https://github.com/openscenegraph/OpenSceneGraph
cd OpenSceneGraph
mkdir build
cd build
cmake ..
make -j4 & sudo make install
```

Then install SUMO
```
sudo apt-get install libfox-1.6-dev swig libgtest-dev libopenthreads-dev default-jdk -y
cd ~/Downloads
wget https://github.com/eclipse/sumo/archive/v0_32_0.tar.gz
tar -xvf v0_32_0.tar.gz
cd sumo-0_32_0/build
cmake ..
make -j4 && sudo make install
```

```
sudo apt-get install cmake python g++ libxerces-c-dev libfox-1.6-dev libgdal-dev libproj-dev libgl2ps-dev swig
git clone --recursive https://github.com/eclipse/sumo
export SUMO_HOME="$PWD/sumo"
mkdir sumo/build/cmake-build && cd sumo/build/cmake-build
cmake ../..
make -j$(nproc)
```
## Basic usage
Use `setup_sumo_env.sh` to setup environment for SUMO. It will install all the necessary libraries and reconfigure the current project folder to serve the plugin (i.e replace almost everything in plugin folder).

```
./setup_sumo_env.sh
```

Use `genOSM.sh` to generate all the files necessary for SUMO to run. It will 

- Generate .osm file
- Generate all the sumo config files (xx.sumo.cfg, xx.net.xml, map.poly.xml, map.rou.xml, trip.trips.xml, vehicles.xml, view.xml)
- Exact lane and intersection information for sumo plugin.

Note that `genOSM.sh` needs to be run on McMaster's network (VPN). Do `./genOSM.sh -h` for more information. Example command:

```
./genOSM.sh -l -75.1 -r -75 -b 45 -t 45.1 -f test.osm -s ~/Downloads/sumo-0.32.0/
```

Use `runSUMO.sh` to start SUMO with plugin specified in `plugin/setting.xml`. It will compile the plugin and link it to SUMO simulation.

```
./runSUMO.sh -g
```

## Compile and Build with TraCI
In order to compile and build the plugin, we need several files from SUMO source code and build, assuming that SUMO is already build on your machine.

In the source code we need the following files and folders. Assuming the source code is located at /sumo/src. Then we need:

   - folder at /sumo/src/foreign
   - folder at /sumo/src/libsumo
   - folder at /sumo/src/traci-server
   - folder at /sumo/src/utils
   - file at /sumo/src/config.h

If SUMO was built correctly, the folders should contain the built file (on linux is .a .o files). This should replace the files/folders in the src folder.

Then to build and run the plugin 
```
g++ -std=c++0x -o test TraCIAPITest.cpp utils/traci/libtraciclient.a foreign/tcpip/storage.o foreign/tcpip/socket.o -I . -lboost_system -lboost_thread -lncurses
./test
```
Since it is a server-client interaction, SUMO has to be running in local server before running the plugin
```
gnome-terminal -x sumo-gui -c config.sumo.cfg --remote-port 1337
```
Or you can use run "build_traci.sh" to build.
```
./build_traci.sh
```
"run.sh" to run SUMO on local server and build and run plugin. Argument ```-n``` means running SUMO WITHOUT GUI, argument ```-g``` runs SUMO with GUI. If you don't need to debug or use User Controllable Vehicle, it is recommend to not use GUI to improve simulation speed.
```
./run.sh -n
./run.sh -g
```
## Settings for SUMO
Since TraCI overwrites most of the settings and configurations define for SUMO, the config files needs to be modified. 

   - Network file - which defines the routes network including length, location, number lanes, intersection ... etc. Since for TraCI, we need to define all the possible roads and route a vehicle can take, hence for simplicity, we define a cross road with 3 lanes.
   - Vehicle file - which defines the types of vehicles that can exist in the simulation. It also defines the length, max speed, safty gap between vehicles, acceleration rate and deceleration rate. Special vehicles that can run a red light is also defined here.
   - Route file - traditionally, this file will have all the routes of all the vehicles will take in the simulation. But since we are using TraCI and also want the simulation to run as long as we want, we do not define as above. What we define is the routes' ID which contains the combination of valid roads/lanes.
   - Setting files (optional) - defines the additional details of the vehicles, such as CO2 emission rate, PMx emission rate, NOx, HC emission rates.
   - Fcd file - the file is generated when simulation starts. It is the "output" of SUMO, which tells us the states of vehicles at specific timestamp.

## Plugin Code
The code consist of normal traffic generation, user vehicle control and anomaly vehicle generation.

The boost::thread is use for controlling user vehicle. It allows us to listen to the user control while simulation continuous.

First time we need to do is to connect to the SUMO local server. Once connect, we can start controlling the simulation.
```
this->connect("localhost", 1337);
```

Once SUMO server and Plugin client are connected, the code will execute reading the ```setting.xml``` file. User can change the parameters within this file to change the behaviour of the simulation. Setting consist of:

 - Simulation Mode
    - real: SUMO simulate with real-time. 1 second SUMO time is 1 second real-time
    - simulate: increment "time" for every 100 SUMO seconds. Options are, "year", "month", "day", "dayweek", "hour" and "min". For example, if we choose "hour", 100 SUMO seconds is 1 hour of real-world time
    - density: SUMO does not change with time and fill the simulation area up to specified density
 - Output file
    - anom: tag the anomaly vehicles simulated based on cumulative anomalies. Meaning a vehicle was simulated as anomaly in anytime during simulation.
    - instance: tag the anomaly vehicles whenever anomaly actions are injected onto a vehicle. Meaning the above tags with time.
 - Special Area
    - Speed limit zone: specify the area and speed the vehicles are limited to
    - high anom: specify the area where vehicle have higher probability of behaving anomaly. When vehicle exit this area, the probability of anomaly is decrease to normal.
 - Normal vehicle
    - specify the probability of generate certain types of vehicle
    - further specification of ```army``` vehicle
 - Anomaly vehicle
    - specify the probability of anomaly events
### Normal Traffic Generation
```
else if(v_type == v_car && distd(genu) <= P(car)){
    if(distd(genu) > uturn_perc){
        this->vehicle.add(std::to_string(++id), "s" + std::to_string(distu(genu)), "car",  std::to_string(-1), std::to_string(distl(genu)));
    }
    else{
        this->vehicle.add(std::to_string(++id), "u" + std::to_string(distut(genu)), "car",  std::to_string(-1), std::to_string(distl(genu)));
    }
}
```
Above the is code to generate a normal vehicle of type "car" with probability of generation 0.75. At every timestep, the type of vehicle is determined uniformly through all the defined vehicle types in the config file, and each type has its own probability of successful generation. Then at each "car" has a probability of taking a u-turn during its trip.

We can change the color of the vehicle by
```
this->vehicle.setColor(special_id, {160, 32, 240, 255});
```

For army/convoy, we define the vehicles to have consecutive vehicle generations. The number of consecutive army/convoy is uniform distributed between 5 and 10.

### User Vehicle Control
We use the boost threading library to acheive continuous simulation while listening to user's control input. Which is defined in 
```
void contsim()
```
User can add, delete, speed up, slow down, take turns, complete stop and fast acceleration. The user vehicle will NOT follow any collision-avoidance algorithm define in SUMO. It WILL collide with others, but other normal vehicles will try to avoid collision to the user vehicle.

Note: Only one user vehicle can exist a one time.

### Anomaly Vehicle Generation
The probability of generating a anomal vehicle is defined at
```
double odd_perc = this->anom_sim_para.anomaly_event_probability;
```
and each type of anomaly is defined at
```
double odd_perc = this->anom_sim_para.anomaly_event_probability;
double fast_lane_perc = this->anom_sim_para.anomaly_lane * odd_perc;
double bad_uturn_perc = this->anom_sim_para.anomaly_uturn * odd_perc;
double speeding_slow_perc = this->anom_sim_para.anomaly_speed * odd_perc;
double off_road_perc = this->anom_sim_para.anomaly_off * odd_perc;
```
We have 4 main anomaly events:
    
    - Frequent lane change
    - U-turn in the middle of the road
    - Speeding, Driving too slow (include complete stop)
    - Driving off-road
Hence all the anomal events are:
    
    - Switching lane very frequently
    - U-turn in the middle of the road
    - Speeding
    - Driving very slow (include complete stop)
    - Move stop frequently
    - Driving off-road, resume back to original lane
    - Driving off-road to avoid traffic light
    

Note 1: Each main anomaly event is independent of each other.

Note 2: Emergency events are considered "normal vehicle generation". For example, ambulance running a red light.

Note 3: If a non-user vehicle collide with others, simulation will return error. So for simplicity, without taking vehicle length and safty gap into account, anomaly events only happen to type "car".

Note 4: All the anomaly vehicles are labelled in "label.txt" with its anomal events

Note 5: All the anomaly vehicles with time are labelled in "label_instance.txt" with its anomal events


### Group Anomaly Vehicle Generation
The probability of generating grouped anomaly vehicles is defined
```
double group_odd_event_perc = odd_event * this->anom_sim_para.anomaly_group_event_probability;
```
and each type of group anomaly is defined as
```
double this->anom_sim_para.anomaly_parallel_army;
double this->anom_sim_para.anomaly_coord_motion;
double this->anom_sim_para.anomaly_drag;
```
hence all the group anomaly events are:
    
    - Drag Racing
    - Convoy
    - Coordinated Motion
 
Note 1: The drag racing event is pre-defined as not all lanes are long enough and straight

Note 2: When vehicles reach the end for drag racing, the scattering is NOT pre-defined

Note 3: The vehicles will only fill the lanes with same direction for convoy event as opposite direction lane driving is not supported in current SUMO version. 

## Troubleshoot
---

### ./configure cannot find proj_api.h
---
Add `--with-proj-include` and `--with-proj-lib`. There are some versioning issue for PROJ. Since `proj_api.h` is soft deprecated for version > 6, but we need version > 6 for GDAL. If still does not work, get the sumo version with with cmake build capabilities.

### xxx is used in project, but is set to NOTFOUND YYY when using cmake
---
Include the path to find library YYY. It is usually installed in `/usr/local`, and cmake isn't told to look there

```
cmake .. -DYYY_INCLUDE_DIR=/usr/local
```

### Error: tcp::Storage::readIsSafe want to read 8 bytes from storage, but only 4 remaining
---
The version of sumo is different from the one you used to compile the plugin. Make sure they are the same version

### Cannot find file xxx when setting up sumo with script
---
Make sure you can getting the exact version of SUMO

### genOSM.sh timed out
--- 
Make sure Osmosis is running on server and your client computer is connected to McMaster's VPN.

### SUMO_HOME not set
---
Run

```
export SUMO_HOME="path/to/sumo/root"
```

Note that the above command needs to be executed for every new terminal

For permanent solution: append the same line to `~/.bashrc` and `~/.profile`

Re-loggin or reboot for the permanent solution to take effect

### Script exit with Permission denied error
---
Run script with `sudo`. Note that some files will need to be demoted manually (such as downloaded zip files, built folders ... etc.)

### gdal/cpl_port.h must have c++11
---
Re-run cmake with `-DCMAKE_CXX_FLAGS=-std=c++11`

### Cannot receive schema from xxx/sumo-0_32_0/data/xsd/additional_file.xsd
---
Make sure your `SUMO_HOME` is set. You can run `echo $SUMO_HOME` to check. Make sure you have permission to access the file. To give all user permission to access a file run:

```
sudo chmod 777 filename
```

To give all files and directory (and sub-directories), run

```
sudo chmod -R 777 *
```

### jmDriveAfterRedTime is not declared in vType, cannot read file vehicle.xml
---
Problem can be solved by setting correct `SUMO_HOME` and proper permission describe above.


### xxx not satisfied constrain ... yyy must not already exist
---
This is resulting from installing PROJ or OSG multiple time. If you are confident that PROJ and OSG are installed properly, you can safely ignore these messages (going to be a lot).

Otherwise you should uninstall the libraries and reinstall.

### double free ... abort ...
---
This issue cannot be resolved by us. Usually newer version of libraries can resolve this issue, but we are constrained by SUMO's version.

Through testing, this message can be ignore.

### Cmake: cannot create target because another target with the same name already exist sumo/src/libsumo ...
---

- Go to `~/Downloads/sumo-0_32_0/src/libsumo` and open `CMakeLists.txt`
- Find line with `JAVA_FOUND AND JNI_FOUND`
- Replace all `libsumo` in this block with `libsumojni`
