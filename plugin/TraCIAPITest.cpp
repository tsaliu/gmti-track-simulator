#include <iostream>
#include <utils/traci/TraCIAPI.h>

#include <boost/thread.hpp>
#include <unistd.h>
#include <stdlib.h>
#include <ncurses.h>
#include <math.h>
#include <string>
#include <random>
#include <chrono>
#include <algorithm>
#include <limits>

#include <time.h>
#include <fstream>
#include <ctime>
#include <cmath>

#include <pugixml.hpp>

#define v_non_classified	0
#define v_car			    1
#define v_truck			    2
#define v_bus			    3
#define v_semitrailer		4
#define v_van			    5
#define v_motorcycle		6
#define v_truck_trailer		7
#define v_car_trailer		8
#define v_emergency		    9
#define v_authority		    10
#define v_army			    11

#define g_army_parallel     0
#define g_drag              1
#define g_coord_motion      2

#define winter              1
#define spring              2
#define summer              3
#define fall                4

#define num_zone            1000

#define school_speed        11.11

/**traffic stats from https://www.vegvesen.no/_attachment/336339/binary/585485*/

struct speed_zone_struct{
    std::string type;
    std::vector<std::string> lane;
    std::vector<double> default_speed;
}speed_zone[num_zone];
class speed_area {
	public:
		std::string type;
		int id;
		double sp, x, y, xlen, ylen;

		speed_area(){}
		speed_area(std::string in_type, int in_id, double in_sp, double in_x, double in_y, double in_xlen, double in_ylen){
			this->type = in_type;
			this->id = in_id;
			this->sp = in_sp;
			this->x = in_x;
			this->y = in_y;
			this->xlen = in_xlen;
			this->ylen = in_ylen;
		}
};
struct normal_simulation_parameters {
	double normal_uturn_rate = 0;
	double non = 0, car = 0, truck = 0, bus = 0, semitrailer = 0, van, moto = 0, trucktrailer = 0, cartrailer = 0, authority = 0, army = 0, emergency = 0;
	double army_min = 0, army_max = 0;
};
struct anomaly_simulation_parameters{
    //Individual anomaly
	double anomaly_event_probability = 0;
	double anomaly_lane = 0, anomaly_uturn = 0, anomaly_speed = 0, anomaly_off = 0;
	double anomaly_speed_sp = 0, anomaly_speed_slow = 0, anomaly_speed_spsl = 0;

    //group anomaly
    double anomaly_group_event_probability = 0;
    double anomaly_drag = 0;
    double anomaly_drag_startx = 0, anomaly_drag_starty = 0, anomaly_drag_endx = 0, anomaly_drag_endy = 0;
    double anomaly_parallel_army = 0;
    double anomaly_coord_motion = 0;
    double coord_min = 0, coord_max = 0;
    double group_wait_time = 0;
};
struct vehicle_para{
    double x, y, ang;
};
struct area{
    double minx, miny, maxx, maxy, xlen, ylen;
};
class timeLog{
    public:
        std::ofstream outfile;
        timeLog(std::string fileName){
            this->outfile.open(fileName);

            std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
            time_t tt = std::chrono::system_clock::to_time_t(now);
            this->outfile << "Start:\t" << ctime(&tt);

        }
        void timeEnd(){
            std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
            time_t tt = std::chrono::system_clock::to_time_t(now);
            this->outfile << "End:\t" << ctime(&tt);
            this->outfile.close();
        }
};


void print_matrix(std::vector<std::string> x){
    for(int i = 0; i<x.size(); i++){
        std::cout << x[i] << "\t";
    }
    std::cout << std::endl;
}

void print_matrix(std::vector<double> x){
    for(int i = 0; i<x.size(); i++){
        std::cout << x[i] << "\t";
    }
    std::cout << std::endl;
}

void print_matrix(std::vector<int> x){
    for(int i = 0; i<x.size(); i++){
        std::cout << x[i] << "\t";
    }
    std::cout << std::endl;
}
double calc_dis(libsumo::TraCIPosition a, libsumo::TraCIPosition b){
    return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
}
int vec_find_min(std::vector<double> x){
    int min_at = 0;
    for(int i = 1; i<x.size(); i++){
       if(x[i] < x[min_at]){
           min_at = i;
       }
    }
    return min_at;
    // return std::distance(x.begin(), std::min_element(x.begin(), x.end()));
}
template <class T>
T calc_sum(std::vector<T> x){
    T result;
    // for(auto xx : x){
    //     result += xx;
    // }
    result = std::accumulate(x.begin(), x.end(), 0);
    return result;
}
int vec_find_max(std::vector<double> x){
    int max_at = 0;
    for(int i = 1; i<x.size(); i++){
       if(x[i] > x[max_at]){
           max_at = i;
       }
    }
    return max_at;
    // return std::distance(x.begin(), std::max_element(x.begin(), x.end()));
}
template <class T>
void file_print(std::vector<T> x, std::string name){
    std::ofstream outfile;
    outfile.open(name);
    for(auto xx : x){
        outfile << xx << "\n";
    }

    outfile.close();
    outfile.clear();
}
class Client : public TraCIAPI {
private:
    struct tm start_localtime;
    /** when to change time in simulation of real time*/
    int duration, duration_count;

    double averageLaneWidth;

    libsumo::TraCIPositionVector lane_shape;
    std::vector<std::string> the_lane;
    std::vector<std::string> all_lanes;

    std::vector<std::string> lane_name_v;
    std::vector<int> num_lane_v;

    std::vector<std::string> stuck_veh;
    std::vector<int> stuck_count;

    /**stuck count*/
    int sc;

    bool stop;
    bool flag;
    bool esc;
    bool del;
    bool add, added, adding;
    bool uturn_b, uturn_e, uturn_prog, uturn_d;
	double uturn_angle_i, uturn_angle_r;
	libsumo::TraCIPosition uturn_p;
	double ux, uy;

	bool user_vehicle;
    std::string strv;
    std::string strr;
    std::string stre;

    std::vector<std::string> l;

    double speed;
    int key;

    libsumo::TraCIPosition p, ap, up;
    double angle, aa, ua;
    double da;
    double x;
    double y;
    double mv;

    int r_id;
    int p_id, dp_id;

    int freq;

    std::vector<double> uturn_speed, uturn_sp_diff, uturn_angle_diff;
    double lane_length;

    int num_veh_per_timestep, tot_num_veh;

    std::vector<std::string> area_id_list, past_area_id_list;

    std::vector<area> group_coord_meet_point;
    std::vector<std::vector<std::string>> coord_id;
    std::vector<double> group_coord_waittime_count;
    std::vector<std::vector<int>> group_coord_arrive;

    std::vector<std::string> drag_id;
    std::vector<area> drag_startend_point;
    std::vector<int> drag_start_arrive, drag_end_arrive;
    double drag_wait_start;
    std::vector<double> drag_speed, drag_accele;
    std::vector<std::vector<double>> originalLaneMaxSpeed;
    std::vector<std::string> passedEdges;
    double customRouteNumber;

public:
    std::vector<std::string> string_parsing(std::string in, std::string delimiter){
        std::vector<std::string> out;
        std::string s = in;
        size_t pos = 0;
        std::string token;
        while((pos = s.find(delimiter)) != std::string::npos){
            token = s.substr(0, pos);
//            std::cout << token << std::endl;
            s.erase(0, pos + delimiter.length());
            out.push_back(token);
        }
        out.push_back(s);
        return out;
    }

    int check_exist(std::vector<std::string> x, std::string to_check){
        for(int i = 0; i<x.size(); i++){
            if(x[i] == to_check){
                return i;
            }
        }
        return -1;
    }
    double calc_avg(std::vector<double> x){
        double out = 0;
        for(int i = 0; i<x.size(); i++){
            out += x[i];
        }
        return out / x.size();
    }
    std::vector<double> vec2perc_vec(std::vector<double> x){
        double avg = this->calc_avg(x);
        std::vector<double> out;
        for(int i = 0; i<x.size(); i++){
            out.push_back(x[i] / avg);
        }
        return out;
    }


    bool find_Keyword(std::ifstream & stream, std::string token) {
        std::string line;
        while (getline(stream, line)) {
            if (line.find(token) != std::string::npos) {
//                std::cout << line << std::endl;
                return true;
            }
        }
//        std::cout << token << " not found" << std::endl;
        return false;
    }
    void init_labelling(std::string file){
        std::ofstream outfile;
        outfile.open(file);
        outfile.close();
    }
    void label_one(std::string file, std::string type, std::string value){
        std::ifstream infile;
        infile.open(file);
        std::string to_find = type + "\t" + value;

        std::ofstream outfile;
        if(!find_Keyword(infile, to_find)){
            infile.close();
            infile.clear();

            outfile.open(file, std::ios_base::app);
            outfile << type << "\t" << value << "\n";
        }
        outfile.close();
        outfile.clear();
    }
    void label_instance(std::string file, double time, std::string type, std::string value){
        std::ifstream infile;
        infile.open(file);
        std::string to_find = std::to_string(time) + "\t" + type + "\t" + value;

        std::ofstream outfile;
        if(!find_Keyword(infile, to_find)){
            infile.close();
            infile.clear();

            outfile.open(file, std::ios_base::app);
            outfile << std::to_string(time) << "\t" << type << "\t" << value << "\n";
        }
        outfile.close();
        outfile.clear();
    }
    void label_user(std::string file, double time, libsumo::TraCIPosition pos, double ang, std::string value){
        std::ofstream outfile;
        outfile.open(file, std::ios_base::app);
        outfile << std::to_string(time) << "\t" << pos.x << "\t" << pos.y << "\t" << ang << "\t" << value << std::endl;
        outfile.close();
        outfile.clear();
    }

    double file_line_count2num_routes(){
        int number_of_lines = 0;
        std::string line;
        std::ifstream myfile("../input/out.xml");

        while (std::getline(myfile, line))
            ++number_of_lines;
//        std::cout << "Number of lines in text file: " << number_of_lines;

        return (double) number_of_lines - 3;
    }
    void num_of_vehicle(double density){
        std::vector<std::string> all_lanes = this->lane.getIDList();
        double tot_lane_length = 0;
        for(int i = 0; i<all_lanes.size(); i++){
            tot_lane_length += this->lane.getLength(all_lanes[i]);
            this->averageLaneWidth += this->lane.getWidth(all_lanes[i]);
        }
        this->freq = 1;
        this->num_veh_per_timestep = 5;
        /** max ~ 1000 vehicles at once*/
        double veh_density = density;
        this->tot_num_veh = std::round(tot_lane_length / 200 * veh_density);

        this->averageLaneWidth /= (double)all_lanes.size();
    }

    void num_of_vehicle(double year, double month, double day, double day_of_week, double hour, double minu){
        std::vector<double> stat_hour = {200, 205, 210, 215, 250, 290, 320, 375, 410, 500, 600, 700, 900, 1100, 830, 700, 600, 580, 500, 400, 420, 450, 550, 590, 610, 615, 550, 500, 470, 460, 600, 680, 770, 790, 785, 780, 700, 630, 560, 500, 400, 350, 300, 280, 210, 200, 180, 190};
        /**sun - sat*/
        std::vector<double> stat_day_of_week = {9800, 10100, 8300, 8500, 8200, 12200, 8100};
        double stat_year = 1500; /** x1.09 per year, since 1992*/
        std::vector<double> stat_month = {3600, 3600, 3600, 3550, 3550, 3500, 3300, 3000, 2800, 2600, 2500, 2650, 2850, 3050, 3300, 3500, 3650, 3800, 3850, 3850, 3850, 3950, 3900, 3900};
//        std::cout << "xxx\t" << stat_month.size() << std::endl;
//    print_matrix(stat_hour);

        std::vector<double> perc_stat_day_of_week = this->vec2perc_vec(stat_day_of_week);
        std::vector<double> perc_stat_month = this->vec2perc_vec(stat_month);
        double perc_stat_year = pow(1.09, year - 1992);
//        std::vector<double> perc_stat_day_of_week = this->vec2perc_vec(stat_day_of_week);



        double year_count = pow(1.09, year - 1992) * stat_year;


        if(minu > 0.5) hour += 0.5;
        double hour_count = stat_hour[hour * 2];

        double day_of_week_count = stat_day_of_week[day_of_week];

        if(day > 15) month += 0.5;
        double month_count = stat_month[(month - 1) * 2];



//        std::cout << "zzzzzz\t"<< year_count << "\t" << month_count << "\t" << day_of_week_count << "\t" << hour_count << std::endl;
//        std::cout << "zzzzzz\t"<< perc_stat_year << "\t" << perc_stat_month[(int) month * 2] << "\t" << perc_stat_day_of_week[day_of_week - 1] << "\t" << hour_count << std::endl;
        this->tot_num_veh = hour_count * perc_stat_month[(month - 1) * 2] * perc_stat_day_of_week[day_of_week];
        this->freq = 1;
        this->num_veh_per_timestep = 1;
    }

    void time_func(){
        time_t now = std::time(NULL);
        struct tm ltm = *std::localtime(&now);
        ltm.tm_isdst = 1;
        std::mktime(&ltm);
        double year = ltm.tm_year + 1900;
        /** 1 - 12*/
        double month = ltm.tm_mon + 1;
        /** 1 - 31*/
        double day = ltm.tm_mday;
        /**mon = 1, sun = 0*/
        double day_of_week = ltm.tm_wday;
        /** 0 - 23*/
        double hour = ltm.tm_hour;
        /** 0 - 59*/
        double minu = ltm.tm_min;

        num_of_vehicle(year, month, day, day_of_week, hour, minu);
        /** use times to control number of vehicles, zones to fetch lanes to reduce max speed allowed*/
//        std::cout << "xxx\t" << ltm.tm_isdst << "\t" << year << "\t" << month << "\t" << day << "\t" << hour << std::endl;
        if(month > 12 || month <= 0){
            std::cout << "Invalid Month. Month should be between 1 - 12" << std::endl;
            exit(EXIT_SUCCESS);
        }
        if(day > 31 || day <= 0){
            std::cout << "Invalid Day. Day should be between 1 - 31" << std::endl;
            exit(EXIT_SUCCESS);
        }
        if(hour > 24 || hour <= 0){
            std::cout << "Invalid Hour. Hour should be between 1 - 24" << std::endl;
            exit(EXIT_SUCCESS);
        }

    }
    void time_func(std::string res){
        if(this->duration_count % this->duration == 0){
            if(res == "hour"){
                this->start_localtime.tm_hour = (this->start_localtime.tm_hour + 1) % 24;
            }
            else if(res == "year"){
                this->start_localtime.tm_year + 1;
            }
            else if(res == "month"){
                this->start_localtime.tm_mon = (this->start_localtime.tm_mon + 1) % 12;
            }
            else if(res == "day"){
                this->start_localtime.tm_mday--;
                this->start_localtime.tm_mday = (this->start_localtime.tm_mday + 1) % 31;
                this->start_localtime.tm_mday++;
            }
            else if(res == "min"){
                this->start_localtime.tm_min = (this->start_localtime.tm_min + 1) % 60;
            }
            else if(res == "dayweek"){
                this->start_localtime.tm_wday = (this->start_localtime.tm_wday + 1) % 7;
            }
            else{
                std::cout << "Invalid Time Parameter" << std::endl;
                exit(EXIT_SUCCESS);
            }

            if(this->start_localtime.tm_hour == 0){
                this->start_localtime.tm_mday--;
                this->start_localtime.tm_mday = (this->start_localtime.tm_mday + 1) % 31;
                this->start_localtime.tm_mday++;
                this->start_localtime.tm_wday = (this->start_localtime.tm_wday + 1) % 7;
            }
            if(this->start_localtime.tm_mday == 1){
                this->start_localtime.tm_mon = (this->start_localtime.tm_mon + 1) % 12;
            }
            if(this->start_localtime.tm_mon == 0){
                this->start_localtime.tm_year++;
            }
        }
        this->duration_count++;

        double year = this->start_localtime.tm_year + 1900;
        double month = this->start_localtime.tm_mon + 1;
        double day = this->start_localtime.tm_mday;

        double day_of_week = this->start_localtime.tm_wday;
        double hour = this->start_localtime.tm_hour;
        double minu = this->start_localtime.tm_min;

        num_of_vehicle(year, month, day, day_of_week, hour, minu);
        if((this->duration_count - 1) % this->duration == 0){

            std::cout << year << "-" << month << "-" << day << " " << hour << "H " << day_of_week << "D " <<"Traffic Volume Changed: " << this->tot_num_veh << std::endl;
        }
        /** use times to control number of vehicles, zones to fetch lanes to reduce max speed allowed*/
//        std::cout << "xxx\t" << this->start_localtime.tm_isdst << "\t" << year << "\t" << month << "\t" << day << "\t" << hour << std::endl;
        if(month > 12 || month <= 0){
            std::cout << "Invalid Month. Month should be between 1 - 12" << std::endl;
            exit(EXIT_SUCCESS);
        }
        if(day > 31 || day <= 0){
            std::cout << "Invalid Day. Day should be between 1 - 31" << std::endl;
            exit(EXIT_SUCCESS);
        }
        if(hour > 23 || hour < 0){
            std::cout << "Invalid Hour. Hour should be between 0 - 23" << std::endl;
            exit(EXIT_SUCCESS);
        }

    }

    void generate_traffic_volume(std::string mode, double density = 0.8, std::string res = "hour"){
        if(mode == "density" || mode == "Density" || mode == "d" || mode == "D"){
            if(density > 1 || density <= 0){
                std::cout << "Invalid Traffic Density" << std::endl;
                exit(EXIT_SUCCESS);
            }

            this->num_of_vehicle(density);
        }
        else if(mode == "real" || mode == "Real" || mode == "r" || mode == "R"){
            this->time_func();
        }
        else if(mode == "simulate" || mode == "Simulate" || mode == "s" || mode == "S"){
            this->time_func(res);
        }
        else{
            std::cout << "Invalid Mode for Generating Traffic Volume" << std::endl;
            exit(EXIT_SUCCESS);
        }
    }

    void extract_num_lanes(){
        std::ifstream readFile("../input/lane.meas");
        std::string line;
        while(std::getline(readFile, line)){
            std::istringstream iss(line);
            std::string lane_name;
            if(std::getline(iss, lane_name, ',')){
                int num_lane;
                if(iss >> num_lane){
                    this->lane_name_v.push_back(lane_name);
                    this->num_lane_v.push_back(num_lane);
//                    std::cout << lane_name <<"\t" << num_lane << std::endl;
                }
            }
        }

    }

    int get_num_lane(std::string lane_name){
        std::vector<std::string> parsed = string_parsing(lane_name, "_");
//        print_matrix(parsed);
//        std::cout << parsed.size() << std::endl;
        std::string find_name;
        if(parsed.size() == 3){
            find_name = parsed[0] + "_" + parsed[1];
        }
        else if(parsed.size() == 2){
            find_name = parsed[0];
        }
        else{
            find_name = lane_name;
        }
//        std::cout << "lane:\t" << find_name << " num:\t" << this->lane_name_v[0] << std::endl;
        for(int i = 0; i<this->lane_name_v.size(); i++){
            if(this->lane_name_v[i] == find_name){
                return this->num_lane_v[i];
            }
        }
        std::cout << "Error: Lane not found for Num Lane:\t" << find_name << std::endl;
        exit(EXIT_SUCCESS);
//        return 0;
    }
    int get_num_lane_nat(std::string lane_name){
//        std::cout << "lane:\t" << find_name << " num:\t" << this->lane_name_v[0] << std::endl;
        for(int i = 0; i<this->lane_name_v.size(); i++){
            if(this->lane_name_v[i] == lane_name){
                return this->num_lane_v[i];
            }
        }

        std::cout << "Error: Lane not found for Num Lane Nat:\t" << lane_name << std::endl;
        exit(EXIT_SUCCESS);
//        return 0;
    }

    /**
        ^--------
        |       |
        |       |
        |       |
        x------->
    return list of lanes
    */

    void define_area(){
        /**school 40km/h 11.11m/s 7:00-17:30 mon-fri sept-june*/
        std::vector<double> zone_at_x, zone_at_y, zone_size_x, zone_size_y;
        std::vector<std::string> zone_type;

        /**school zone x, y, rectangular*/
        for(int i = 0; i<this->defined_limit_area.size(); i++){
        	zone_at_x.push_back(defined_limit_area[i].x);
	        zone_at_y.push_back(defined_limit_area[i].y);
	        zone_size_x.push_back(defined_limit_area[i].xlen);
	        zone_size_y.push_back(defined_limit_area[i].ylen);
	        zone_type.push_back(defined_limit_area[i].type);
        }
        /**school zone x, y, rectangular*/
        // zone_at_x.push_back(1358.96);
        // zone_at_y.push_back(1867.01);
        // zone_size_x.push_back(520);
        // zone_size_y.push_back(565);
        // zone_type.push_back("school");

        this->all_lanes = this->lane.getIDList();
//        std::cout << "one" << this->lane.getEdgeID(this->all_lanes[0]) <<std::endl;
        for(int i = 0; i<this->all_lanes.size(); i++){
            this->lane_shape = this->lane.getShape(this->all_lanes[i]);
//            std::cout << "lane:\t" << this->all_lanes[i] << " shape:\t" << this->lane_shape.size() << std::endl;
            for(int k = 0; k<this->lane_shape.size(); k++){
                for(int j = 0; j<zone_at_x.size(); j++){
                    if((this->lane_shape[k].x >= zone_at_x[j] && this->lane_shape[k].x <= zone_at_x[j] + zone_size_x[j]) && (this->lane_shape[k].y >= zone_at_y[j] && this->lane_shape[k].y <= zone_at_y[j] + zone_size_y[j])){
//                        this->the_lane.push_back(this->all_lanes[i]);
                        speed_zone[j].lane.push_back(this->all_lanes[i]);
                        speed_zone[j].type = zone_type[j];
                        speed_zone[j].default_speed.push_back(this->lane.getMaxSpeed(this->all_lanes[i]));
                    }
                }
            }
//            if(get_num_lane(all_lanes[i]) != 0)
//                std::cout << "lane:\t" << this->all_lanes[i] << " num:\t" << get_num_lane(all_lanes[i]) << std::endl;

        }

    }

    std::string getNearestLane(double x, double y){
        std::vector<std::string> alllanes = this->lane.getIDList();
        libsumo::TraCIPosition loc;
        loc.x = x;
        loc.y = y;
        std::vector<double> allmindis;
        for(int i = 0; i<alllanes.size(); i++){
            if(alllanes[i].find(":") == std::string::npos){
                libsumo::TraCIPositionVector theshape = this->lane.getShape(alllanes[i]);
                std::vector<double> dis;
                for(int i = 0; i<theshape.size(); i++){
                    dis.push_back(calc_dis(loc, theshape[i]));
                }
                allmindis.push_back(dis[vec_find_min(dis)]);
            }
            else if(alllanes[i].find("6") != 0){
                libsumo::TraCIPositionVector theshape = this->lane.getShape(alllanes[i]);
                std::vector<double> dis;
                for(int i = 0; i<theshape.size(); i++){
                    dis.push_back(calc_dis(loc, theshape[i]));
                }
                allmindis.push_back(dis[vec_find_min(dis)]);
            }
            else if(alllanes[i].find("-") == 0 && alllanes[i].find("6") != 1){
                libsumo::TraCIPositionVector theshape = this->lane.getShape(alllanes[i]);
                std::vector<double> dis;
                for(int i = 0; i<theshape.size(); i++){
                    dis.push_back(calc_dis(loc, theshape[i]));
                }
                allmindis.push_back(dis[vec_find_min(dis)]);
            }
            else{
                allmindis.push_back(std::numeric_limits<double>::max());
            }
        }
        return alllanes[vec_find_min(allmindis)];
    }
    std::string getNearestEdge(double x, double y){
        std::string nlane = this->getNearestLane(x, y);
        // std::vector<std::string> parsed = string_parsing(nlane, "_");
        // return parsed[0];
        return this->lane.getEdgeID(nlane);
    }
    std::string getNearestEdge(libsumo::TraCIPosition in){
        std::string nlane = this->getNearestLane(in.x, in.y);
        // std::vector<std::string> parsed = string_parsing(nlane, "_");
        // return parsed[0];
        return this->lane.getEdgeID(nlane);
    }
    std::string getRouteIDfromstartEdge(std::string edge){
        double num_routes = this->file_line_count2num_routes();
        std::vector<std::string> matchRouteID;
        for(int i = 1; i<=num_routes; i++){
            std::vector<std::string> list_edges = this->route.getEdges(std::to_string(i));
            if(edge == list_edges[0]){
                matchRouteID.push_back(std::to_string(i));
            }
        }
        if(matchRouteID.size() > 0){
            std::default_random_engine gen;
            gen.seed(std::chrono::system_clock::now().time_since_epoch().count());
            std::uniform_int_distribution<int> dist_mRoute(0, matchRouteID.size() - 1);
            return matchRouteID[dist_mRoute(gen)];
        }
        else{
            return std::to_string(-1);
        }
    }
    std::string getRouteIDfromendEdge(std::string edge){
        double num_routes = this->file_line_count2num_routes();
        std::vector<std::string> matchRouteID;
        for(int i = 1; i<=num_routes; i++){
            std::vector<std::string> list_edges = this->route.getEdges(std::to_string(i));
            std::vector<std::string>::iterator eit = std::find(list_edges.begin(), list_edges.end(), edge);
            // if(edge == list_edges.back()){
            if(eit != list_edges.end()){
                matchRouteID.push_back(std::to_string(i));
            }
        }
        if(matchRouteID.size() > 0){
            std::default_random_engine gen;
            gen.seed(std::chrono::system_clock::now().time_since_epoch().count());
            std::uniform_int_distribution<int> dist_mRoute(0, matchRouteID.size() - 1);
            return matchRouteID[dist_mRoute(gen)];
        }
        else{
            return std::to_string(-1);
        }
    }
    std::string getRouteIDfromstartendEdge(std::string startEdge, std::string endEdge){
        double num_routes = this->file_line_count2num_routes();
        std::vector<std::string> matchRouteID_start;

        for(int i = 1; i<=num_routes; i++){
            std::vector<std::string> list_edges = this->route.getEdges(std::to_string(i));
            if(startEdge == list_edges[0]){
                matchRouteID_start.push_back(std::to_string(i));
            }
        }

        std::vector<std::string> matchRouteID;
        for(int i =1; i<matchRouteID_start.size(); i++){
            std::vector<std::string> list_edges = this->route.getEdges(matchRouteID_start[i]);
            std::vector<std::string>::iterator eit = std::find(list_edges.begin(), list_edges.end(), endEdge);

            if(eit != list_edges.end()){
                matchRouteID.push_back(matchRouteID_start[i]);
            }
        }

        if(matchRouteID.size() > 0){
            std::default_random_engine gen;
            gen.seed(std::chrono::system_clock::now().time_since_epoch().count());
            std::uniform_int_distribution<int> dist_mRoute(0, matchRouteID.size() - 1);

            return matchRouteID[dist_mRoute(gen)];
        }
        else{
            return std::to_string(-1);
        }
    }
    std::string genRoutestartfromCoord_Straight(libsumo::TraCIPosition start, libsumo::TraCIPosition end){
        std::string RouteID;
        double xlen = abs(start.x - end.x);
        double ylen = abs(start.y - end.y);

        int nSteps = 0;
        if(xlen > ylen){
            nSteps = (int) xlen / 2;
        }
        else{
            nSteps = (int) ylen / 2;
        }
        double xStep = (end.x - start.x) / (nSteps - 1);
        double yStep = (end.y - start.y) / (nSteps - 1);

        std::vector<std::string> ListofEdges;
        std::vector<double> xlist, ylist;
        double nowX = start.x, nowY = start.y;
        for(int i = 0; i<nSteps; i++){
            std::string nEdge = getNearestEdge(start.x + (i * xStep), start.y + (i * yStep));
            if(nEdge.find(":") == std::string::npos){
                ListofEdges.push_back(nEdge);
            }

            // xlist.push_back(start.x + (i * xStep));
            // ylist.push_back(start.y + (i * yStep));
        }
        file_print(ListofEdges, "aa.txt");
        std::vector<std::string>::iterator it = std::unique(ListofEdges.begin(), ListofEdges.end());
        ListofEdges.resize(std::distance(ListofEdges.begin(), it));

        // for(int i = 0; i<ListofEdges.size(); i++){
        //     if(ListofEdges[i].find(":") != std::string::npos){
        //         ListofEdges.erase(ListofEdges.begin() + i);
        //     }
        // }
        file_print(ListofEdges, "ee.txt");
        RouteID = "c" + std::to_string(++this->customRouteNumber);
        this->route.add(RouteID, ListofEdges);

        return RouteID;
    }
    bool checkVehLaneClear(std::vector<std::string> id){
        std::vector<std::string> lane_to_check;
        for(int i = 0; i<id.size(); i++){
            lane_to_check.push_back(this->vehicle.getLaneID(id[i]));
        }

        std::vector<std::string> vlist = this->vehicle.getIDList();
        /**excluding these vehicles*/
        for(int i = 0; i<id.size(); i++){
            std::vector<std::string>::iterator find_id_in_vlist = std::find(vlist.begin(), vlist.end(), id[i]);
            vlist.erase(vlist.begin() + (find_id_in_vlist - vlist.begin()));
        }
        for(int i = 0; i<vlist.size(); i++){
            std::vector<std::string>::iterator lit = std::find(lane_to_check.begin(), lane_to_check.end(), this->vehicle.getLaneID(vlist[i]));

            if(lit != lane_to_check.end()){
                return false;
            }
            else{
                return true;
            }
        }
    }
    void control_area_bytime(){
        double year = this->start_localtime.tm_year + 1900;
        double month = this->start_localtime.tm_mon + 1;
        double day = this->start_localtime.tm_mday;

        double day_of_week = this->start_localtime.tm_wday;
        double hour = this->start_localtime.tm_hour;
        double minu = this->start_localtime.tm_min;

        if(minu > 30){
            hour += 0.5;
        }

        /**school 40km/h 11.11m/s 7:00-17:30 mon-fri sept-june*/
        int at_now = 0;
        while(speed_zone[at_now].lane.size() != 0){
            if(speed_zone[at_now].type == "school"){
                if(month >= 9 || month <= 6){
                    if(day_of_week >= 1 && day_of_week <= 5){
                        if(hour >= 7 && hour <= 17.5){
                            for(int i = 0; i<speed_zone[at_now].lane.size(); i++){
                                this->lane.setMaxSpeed(speed_zone[at_now].lane[i], school_speed);
                            }
                        }
                        else{
		                	for(int i = 0; i<speed_zone[at_now].lane.size(); i++){
		                        this->lane.setMaxSpeed(speed_zone[at_now].lane[i], speed_zone[at_now].default_speed[i]);
		                    }
		                }
                    }
                    else{
	                	for(int i = 0; i<speed_zone[at_now].lane.size(); i++){
	                        this->lane.setMaxSpeed(speed_zone[at_now].lane[i], speed_zone[at_now].default_speed[i]);
	                    }
	                }
                }
                else{
                	for(int i = 0; i<speed_zone[at_now].lane.size(); i++){
                        this->lane.setMaxSpeed(speed_zone[at_now].lane[i], speed_zone[at_now].default_speed[i]);
                    }
                }
            }
            at_now++;
        }
    }

    void control_vehicle_bytime_byzone(std::vector<std::string> all_veh, std::vector<std::string> fast_veh){
        double year = this->start_localtime.tm_year + 1900;
        double month = this->start_localtime.tm_mon + 1;
        double day = this->start_localtime.tm_mday;

        double day_of_week = this->start_localtime.tm_wday;
        double hour = this->start_localtime.tm_hour;
        double minu = this->start_localtime.tm_min;

        if(minu > 30){
            hour += 0.5;
        }

        /**school 40km/h 11.11m/s 7:00-17:30 mon-fri sept-june*/
        int at_now = 0;
        while(speed_zone[at_now].lane.size() != 0){
            if(speed_zone[at_now].type == "school"){
                if(month >= 9 || month <= 6){
                    if(day_of_week >= 1 && day_of_week <= 5){
                        if(hour >= 7 && hour <= 17.5){
                            for(int i = 0; i<all_veh.size(); i++){
                                int exist_fast = check_exist(fast_veh, all_veh[i]);
                                /**if vehicle not with speeding mode*/
                                if(exist_fast == -1){
                                    int exist_lane = check_exist(speed_zone[at_now].lane, this->vehicle.getLaneID(all_veh[i]));
                                    /**if vehicle within school zone and not in speeding mode*/
                                    if(exist_lane >= 0){
                                        this->vehicle.setMaxSpeed(all_veh[i], school_speed);
                                    }
                                    else if(exist_lane == -1){
                                        std::string veh_type = this->vehicle.getTypeID(all_veh[i]);
                                        double type_maxspeed = this->vehicletype.getMaxSpeed(veh_type);
                                        this->vehicle.setMaxSpeed(all_veh[i], type_maxspeed);

                                    }
                                }
                            }
                        }
                    }
                }
            }
            at_now++;
        }
    }
    std::vector<std::string> find_vehicle_within(std::vector<std::string> vlist){
        std::vector<std::string> out;
        for(int i = 0; i<vlist.size(); i++){
            libsumo::TraCIPosition position = this->vehicle.getPosition(vlist[i]);
            // std::cout << position.x << "\t" << position.y << std::endl;
            for(int k = 0; k<this->define_anom_area.size(); k++){
                if(position.x >= this->define_anom_area[k].x && position.x <= this->define_anom_area[k].x + this->define_anom_area[k].xlen){
                    if(position.y >= this->define_anom_area[k].y && position.y <= this->define_anom_area[k].y + this->define_anom_area[k].ylen){
                        out.push_back(vlist[i]);
                    }
                }

            }
        }
        return out;
    }
    bool if_vehilce_in_area(area a, libsumo::TraCIPosition in){
        if(in.x >= a.minx && in.x <= a.maxx){
            if(in.y >= a.miny && in.y <= a.maxy){
                return true;
            }
        }
        // std::cout << in.x << "\t" << in.y << std::endl;
        // std::cout << a.minx << "\t" << a.maxx << "\t" << a.miny << "\t" << a.maxy << std::endl;
        return false;
    }
    libsumo::TraCIPosition randomPointOnLane(std::string &at_lane){
        std::vector<std::string> alllanes = this->lane.getIDList();
        for(int i = 0; i<alllanes.size(); i++){
            if(alllanes[i].find(":") != std::string::npos){
                alllanes.erase(alllanes.begin() + i);
            }
            else if(alllanes[i].find("6") == 0){
                alllanes.erase(alllanes.begin() + i);
            }
            else if(alllanes[i].find("-") == 0){
                if(all_lanes[i].find("6") == 1){
                    alllanes.erase(alllanes.begin() + i);
                }
            }
        }
        std::default_random_engine genr;
        genr.seed(std::chrono::system_clock::now().time_since_epoch().count());
        std::uniform_int_distribution<int> dist_alllanes(0, all_lanes.size() - 1);

        std::string use_lane = alllanes[dist_alllanes(genr)];
        at_lane = use_lane;
        libsumo::TraCIPositionVector theshape = this->lane.getShape(use_lane);

        if(theshape.size() <= 3){
            std::vector<double> ax, ay;
            for(int i = 0; i<theshape.size(); i++){
                ax.push_back(theshape[i].x);
                ay.push_back(theshape[i].y);
            }
            double xlen = ax[vec_find_max(ax)] - ax[vec_find_min(ax)];
            double ylen = ay[vec_find_max(ay)] - ay[vec_find_min(ay)];

            int nSteps = 0;
            if(xlen > ylen){
                nSteps = (int) xlen;
            }
            else{
                nSteps = (int) ylen;
            }
            double xStep = xlen / (nSteps - 1);
            double yStep = ylen / (nSteps - 1);
            libsumo::TraCIPositionVector out_vec;
            for(int i = 0; i< nSteps; i++){
                libsumo::TraCIPosition out;
                out.x = ax[vec_find_min(ax)] + i * xStep;
                out.y = ay[vec_find_min(ay)] + i * yStep;

                out_vec.push_back(out);
            }

            std::uniform_int_distribution<int> dist_out(0, out_vec.size() - 1);
            return out_vec[dist_out(genr)];
        }
        else{
            std::uniform_int_distribution<int> dist_theshape(0, theshape.size() - 1);
            return theshape[dist_theshape(genr)];
        }




    }
    area find_simulation_area(){
        area out_area;
        std::vector<std::string> alllanes = this->lane.getIDList();
        double max_x, max_y, min_x, min_y, xlen, ylen;
        std::vector<double> xall, yall;
        for(int i = 0; i<alllanes.size(); i++){
            libsumo::TraCIPositionVector theshape = this->lane.getShape(alllanes[i]);
            std::vector<double> txall, tyall;
            for(int k = 0; k<theshape.size(); k++){
                txall.push_back(theshape[k].x);
                tyall.push_back(theshape[k].y);
            }
            xall.push_back(txall[vec_find_max(txall)]);
            xall.push_back(txall[vec_find_min(txall)]);
            yall.push_back(tyall[vec_find_max(tyall)]);
            yall.push_back(tyall[vec_find_min(tyall)]);
        }
        double scale = 1;
        min_x = xall[vec_find_min(xall)] * scale;
        max_x = xall[vec_find_max(xall)] * scale;
        max_y = yall[vec_find_max(yall)] * scale;
        min_y = yall[vec_find_min(yall)] * scale;

        out_area.xlen = max_x - min_x;
        out_area.ylen = max_y - min_y;
        out_area.minx = min_x;
        out_area.miny = min_y;
        out_area.maxx = max_x;
        out_area.maxy = max_y;
        return out_area;
    }

    void loopsim () {
        /**getting start time*/
        time_t now = std::time(NULL);
        this->start_localtime = *std::localtime(&now);
        this->start_localtime.tm_isdst = 1;
        std::mktime(&this->start_localtime);


        area whole_area = find_simulation_area();

        std::string& rID = this->strr;
        std::string& vID = this->strv;
        std::string& eID = this->stre;


        std::vector<std::string> l;
        this->speed = 0;

        std::default_random_engine genu;
        genu.seed(std::chrono::system_clock::now().time_since_epoch().count());
        std::uniform_int_distribution<int> distu(1, this->file_line_count2num_routes());
        std::uniform_int_distribution<int> distv(0, this->vehicle_type_count - 1);
        std::uniform_int_distribution<int> distl(0, 2);
        std::uniform_int_distribution<int> distut(1, 4);
        std::uniform_int_distribution<int> distb(0, 1);
        std::uniform_real_distribution<double> distd(0, 1);

        std::uniform_real_distribution<double> distgx(whole_area.minx, whole_area.maxx);
        std::uniform_real_distribution<double> distgy(whole_area.miny, whole_area.maxy);
        std::cout << this->anom_sim_para.coord_min << "\t" << this->anom_sim_para.coord_max << std::endl;
        std::uniform_int_distribution<int> distcoord(this->anom_sim_para.coord_min, this->anom_sim_para.coord_max);

        std::uniform_int_distribution<int> distg(0, 2);

        /**in sumo, default max acceleration is 6m/s*/
        std::uniform_real_distribution<double> distaccele_time(2.5 * 3, 3.7 * 3); //sec to accele to 60miles/h ~100km/h
        std::normal_distribution<double> distaccele(this->vehicletype.getAccel("car") * 0.8 - (this->vehicletype.getAccel("car") * 0.2), (this->vehicletype.getAccel("car") * 0.2) / 3);

        std::uniform_int_distribution<int> distarmy(this->norm_sim_para.army_min, this->norm_sim_para.army_max);
        bool army_start = false;
        bool group_army_start = false;
        bool group_coord_start = false;
        bool group_coord_startgen = false;
        bool drag_start = false;
        bool drag_startgen = false;
        bool drag_race_start = false;
        std::string drag_route;

        int num_army = distarmy(genu);
        std::string army_route;
        std::string army_lane;

        int id = 0;
        int c = 0;
        double uturn_perc = this->norm_sim_para.normal_uturn_rate;

        double odd_perc = this->anom_sim_para.anomaly_event_probability;
        double fast_lane_perc = this->anom_sim_para.anomaly_lane * odd_perc;
        double bad_uturn_perc = this->anom_sim_para.anomaly_uturn * odd_perc;
        double speeding_slow_perc = this->anom_sim_para.anomaly_speed * odd_perc;
        double off_road_perc = this->anom_sim_para.anomaly_off * odd_perc;

        double off_road_pos_perc = 0.3;
        double off_per_pos_perc = 0.6;
        double uturn_pos_perc = 0.2;
        int off_str_duration = 50;
        int off_per_duration = 9999;

        std::vector<std::string> fast_lane_list;
        std::vector<int> fast_lane_plane;
        std::vector<std::string> bad_uturn_list;
        std::vector<std::string> speed_list, slow_list, speed_slow_list;
        std::vector<std::string> off_str_list, off_per_list;
        std::vector<int> bad_uturn_on_lane;
        std::vector<double> bad_uturn_at_pos;
        std::vector<int> bad_uturn_veh_atlane_now;
        std::vector<int> bad_uturn_state;
        std::vector<bool> bad_uturn_progress;
        std::vector<double> speed_list_speed, slow_list_slow;
        std::vector<bool> off_str_progress, off_per_progress;
        std::vector<int> off_str_state, off_per_state;

        std::vector<vehicle_para> army_inline_para;
        std::vector<std::string> army_id;
        std::vector<std::vector<std::string>> group_army_id;
        std::vector<std::vector<vehicle_para>> group_army_inline_para;
        std::vector<std::vector<bool>> group_army_id_added;
        std::vector<std::vector<bool>> group_army_id_then_removed;

        std::vector<std::string> list_pID, list_dpID;

        if(this->user_vehicle){
            this->strv = "R" + std::to_string(++this->r_id);
            this->vehicle.add(this->strv, std::to_string(distu(genu)), "car");
            /**red*/
            this->vehicle.setColor(this->strv, {255, 0, 0, 255});
            std::cout << "User Vehicle Added: " << this->strv << std::endl;
        }

        init_labelling(this->file_label);
        init_labelling(this->file_label_instance);
        init_labelling(this->file_label_user);

        this->define_area();
        this->extract_num_lanes();

        timeLog log_time(this->file_time_log);
        while(!this->esc){
//        this->time_func("hour");
    		this->simulationStep();
            this->generate_traffic_volume(this->simulation_mode, this->simulation_density , this->simulation_increment);
            this->control_area_bytime();

            this->past_area_id_list = this->area_id_list;
            this->area_id_list = find_vehicle_within(this->vehicle.getIDList());

            double odd_event_perc = distd(genu);
            double odd_event_return_perc = distd(genu);
            double group_odd_event_perc = distd(genu);

            if(!flag){
                if(user_vehicle){
                    this->mv = this->vehicle.getMaxSpeed(vID);
                    this->angle = this->vehicle.getAngle(vID);
                    this->da = this->angle;
                    this->flag = true;
                }
                this->flag = true;
            }
            else{
                if(!this->del && this->added){
                    if(user_vehicle)
                        this->p = this->vehicle.getPosition(vID);
                }
                int v_type = distv(genu);
                int a_type = distg(genu);
                if(army_start && !group_army_start){
                    v_type = v_army;
                    group_odd_event_perc = 1;
                }
                else if(!army_start && group_army_start){
                    group_odd_event_perc = 0;
                    a_type = g_army_parallel;
                }
                else if(group_coord_start || group_coord_startgen){
                    group_odd_event_perc = 0;
                    a_type = g_coord_motion;
                }
                else if(drag_start || drag_startgen){
                    group_odd_event_perc = 0;
                    a_type = g_drag;
                }
                if(c % freq == 0 && this->vehicle.getIDList().size() <= this->tot_num_veh && group_odd_event_perc >= this->anom_sim_para.anomaly_group_event_probability){
//                std::cout << "HERE111" << std::endl;
                    if(v_type == v_non_classified && distd(genu) <= this->norm_sim_para.non){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "non_classified",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "non_classified",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_car && distd(genu) <= this->norm_sim_para.car){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "car",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "car",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_truck && distd(genu) <= this->norm_sim_para.truck){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "truck",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "truck",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_bus && distd(genu) <= this->norm_sim_para.bus){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "bus",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "bus",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_semitrailer && distd(genu) <= this->norm_sim_para.semitrailer){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "semitrailer",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "semitrailer",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_van && distd(genu) <= this->norm_sim_para.van){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "van",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "van",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_motorcycle && distd(genu) <= this->norm_sim_para.moto){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "motorcycle",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "motorcycle",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_truck_trailer && distd(genu) <= this->norm_sim_para.trucktrailer){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "truck_trailer",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "truck_trailer",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_car_trailer && distd(genu) <= this->norm_sim_para.cartrailer){
                        this->vehicle.add(std::to_string(++id), std::to_string(distu(genu)), "car_trailer",  std::to_string(-1));
                        for(int i = 1; i<this->num_veh_per_timestep; i++){
                            std::string temp_route = std::to_string(distu(genu));
                            if(id - 1 > 0)
                                if(this->vehicle.getRouteID(std::to_string(id - 1)) != temp_route)
                                    this->vehicle.add(std::to_string(++id), temp_route, "car_trailer",  std::to_string(-1));
                        }
                    }
                    else if(v_type == v_emergency && distd(genu) <= this->norm_sim_para.emergency){
                        std::string special_id = std::to_string(++id);
                        this->vehicle.add(special_id, std::to_string(distu(genu)), "emergency",  std::to_string(-1));

                        /**teal*/
                        this->vehicle.setColor(special_id, {0, 255, 255, 255});


                    }
                    else if(v_type == v_authority && distd(genu) <= this->norm_sim_para.authority){

                        std::string special_id = std::to_string(++id);
                        this->vehicle.add(special_id, std::to_string(distu(genu)), "authority",  std::to_string(-1));

                        /**blue*/
                        this->vehicle.setColor(special_id, {0, 0, 255, 255});
                    }
                    else if(v_type == v_army && distd(genu) <= this->norm_sim_para.army || (army_start && num_army >= 0)){
                        std::string special_id = std::to_string(++id);
                        army_id.push_back(special_id);

                        if(!army_start){
                            army_lane = std::to_string(distl(genu));
                            this->vehicle.add(special_id, std::to_string(distu(genu)), "army",  std::to_string(-1));
                        }

                        if(!army_start){
                            army_route = this->vehicle.getRouteID(special_id);
                        }
                        else if(army_start){
                            this->vehicle.add(special_id, army_route, "army",  std::to_string(-1));
                        }
                        /**purple*/
                        this->vehicle.setColor(special_id, {160, 32, 240, 255});

                        army_start = true;
                        num_army--;
                        if(num_army == 0){
                            num_army = distarmy(genu);
                            army_start = false;
                        }
                    }
                    /**army follow inline*/
                    for(int i = 0; i<army_id.size(); i++){
                        std::vector<std::string> v_list = this->vehicle.getIDList();
                        std::vector<std::string>::iterator vit = std::find(v_list.begin(), v_list.end(), army_id[i]);
                        if(vit != v_list.end()){

                            vehicle_para leading_vehcile_now;
                            vehicle_para vehicle_now;
                            this->ap = this->vehicle.getPosition(army_id[i]);
                            leading_vehcile_now.x = this->ap.x;
                            leading_vehcile_now.y = this->ap.y;
                            this->aa = this->vehicle.getAngle(army_id[i]);
                            leading_vehcile_now.ang = this->aa;

                            army_inline_para.push_back(leading_vehcile_now);

                            if(i > 0){
                                this->vehicle.moveToXY(army_id[i], "", 0, army_inline_para[i - 1].x, army_inline_para[i - 1].y, army_inline_para[i - 1].ang, 2);

                            }
                        }
                        else{
                            army_id.erase(army_id.begin() + i);
                            if(i + 1 < army_id.size()){
                                this->vehicle.moveToXY(army_id[i], "", 0, army_inline_para[i - 1].x, army_inline_para[i - 1].y, army_inline_para[i - 1].ang, 1);
                            }
                        }
                        if(army_id.size() <= 0){
                            army_inline_para.clear();
                        }
                    }
                }
                else if(c % freq == 0 && group_odd_event_perc < this->anom_sim_para.anomaly_group_event_probability){
                    /**this is group*/
                    /**if performing group, disable individual anomaly*/
                    odd_event_perc = 0;

                    if(a_type == g_army_parallel && distd(genu) <= this->anom_sim_para.anomaly_parallel_army || (group_army_start && num_army > 0)){
                        int num_lane = 0;
                        int lane_index_now = 0;
                        if(!group_army_start){
                            std::string special_id = std::to_string(++id);
                            army_lane = std::to_string(distl(genu));
                            this->vehicle.add(special_id, std::to_string(distu(genu)), "army",  std::to_string(-1), std::to_string(0));
                            army_route = this->vehicle.getRouteID(special_id);
                            this->vehicle.setColor(special_id, {153, 0, 76, 255});

                            std::vector<std::string> now_route = this->route.getEdges(army_route);
                            // num_lane = this->get_num_lane(this->vehicle.getLaneID(special_id));
                            num_lane = this->get_num_lane(now_route[0]);
                            lane_index_now = this->vehicle.getLaneIndex(special_id);


                            std::vector<std::string> temp_1army_id;
                            std::vector<bool> temp_added;
                            temp_1army_id.push_back(special_id);
                            temp_added.push_back(false);
                            group_army_id.push_back(temp_1army_id);
                            group_army_id_added.push_back(temp_added);
                            group_army_id_then_removed.push_back(temp_added);


                            for(int l = 0; l<num_lane - 1; l++){
                                std::vector<std::string> temp_army_id;
                                std::vector<bool> ttemp_added;
                                temp_army_id.push_back(std::to_string(++id));
                                ttemp_added.push_back(false);
                                group_army_id.push_back(temp_army_id);
                                group_army_id_added.push_back(ttemp_added);
                                group_army_id_then_removed.push_back(ttemp_added);
                            }

                            // std::vector<std::string> now_route = this->route.getEdges(army_route);
                            for(int l = 1; l<group_army_id.size(); l++){
                                this->vehicle.add(group_army_id[l].back(), army_route, "army", std::to_string(-1), std::to_string(l));
                                // this->vehicle.moveTo(group_army_id[l].back(), now_route[0] + "_" + std::to_string(l), this->vehicle.getLanePosition(group_army_id[l].back()));
                                this->vehicle.setColor(group_army_id[l].back(), {153, 0, 76, 255});
                            }
                            for(int l = 0; l<group_army_id.size(); l++){
                                vehicle_para vempty;
                                std::vector<vehicle_para> temp_vempty;
                                temp_vempty.push_back(vempty);
                                group_army_inline_para.push_back(temp_vempty);
                            }
                        }
                        else if(group_army_start && num_army > 0){
                            for(int l = 0; l<group_army_id.size(); l++){
                                group_army_id[l].push_back(std::to_string(++id));
                                this->vehicle.add(group_army_id[l].back(), army_route, "army",  std::to_string(-1), std::to_string(l));
                                this->vehicle.setColor(group_army_id[l].back(), {153, 0, 76, 255}); //this is wine red
                            }
                        }
                        /**purple*/
                        // this->vehicle.setColor(special_id, {160, 32, 240, 255});

                        group_army_start = true;
                        num_army--;
                        if(num_army == 0){
                            // num_army = distarmy(genu);
                            // group_army_start = false;
                            // group_army_id.clear();
                            // group_army_inline_para.clear();
                        }

                    }
                    else if(a_type == g_drag && distd(genu) <= this->anom_sim_para.anomaly_drag || drag_start || drag_startgen || drag_race_start){
                        if(!drag_start && !drag_startgen){
                            libsumo::TraCIPosition startxy, endxy;
                            startxy.x = this->anom_sim_para.anomaly_drag_startx;
                            startxy.y = this->anom_sim_para.anomaly_drag_starty;
                            endxy.x = this->anom_sim_para.anomaly_drag_endx;
                            endxy.y = this->anom_sim_para.anomaly_drag_endy;

                            std::string startLane = getNearestLane(this->anom_sim_para.anomaly_drag_startx, this->anom_sim_para.anomaly_drag_starty);
                            std::string startEdge = getNearestEdge(this->anom_sim_para.anomaly_drag_startx, this->anom_sim_para.anomaly_drag_starty);

                            std::string endLane = getNearestLane(this->anom_sim_para.anomaly_drag_endx, this->anom_sim_para.anomaly_drag_endy);
                            std::string endEdge = getNearestEdge(this->anom_sim_para.anomaly_drag_endx, this->anom_sim_para.anomaly_drag_endy);

                            area tempa, tempb;
                            int nLane = get_num_lane(startLane);
                            tempa.minx = startxy.x - (nLane + 1) * this->averageLaneWidth;
                            tempa.miny = startxy.y - (nLane + 1)* this->averageLaneWidth;
                            tempa.xlen = this->averageLaneWidth * (nLane + 3);
                            tempa.ylen = this->averageLaneWidth * (nLane + 3);
                            tempa.maxx = tempa.minx + tempa.xlen;
                            tempa.maxy = tempa.miny + tempa.ylen;

                            nLane = get_num_lane(endLane);
                            tempb.minx = endxy.x - (nLane + 1) * this->averageLaneWidth;
                            tempb.miny = endxy.y - (nLane + 1)* this->averageLaneWidth;
                            tempb.xlen = this->averageLaneWidth * (nLane + 3);
                            tempb.ylen = this->averageLaneWidth * (nLane + 3);
                            tempb.maxx = tempb.minx + tempb.xlen;
                            tempb.maxy = tempb.miny + tempb.ylen;

                            this->drag_startend_point = {tempa, tempb};

                            libsumo::TraCIPosition sa, sb, sc, sd;
                            sa.x = tempa.minx;
                            sa.y = tempa.miny;
                            sb.x = tempa.minx;
                            sb.y = tempa.maxy;
                            sc.x = tempa.maxx;
                            sc.y = tempa.maxy;
                            sd.x = tempa.maxx;
                            sd.y = tempa.miny;

                            libsumo::TraCIPosition ea, eb, ec, ed;
                            ea.x = tempb.minx;
                            ea.y = tempb.miny;
                            eb.x = tempb.minx;
                            eb.y = tempb.maxy;
                            ec.x = tempb.maxx;
                            ec.y = tempb.maxy;
                            ed.x = tempb.maxx;
                            ed.y = tempb.miny;

                            libsumo::TraCIPositionVector startshape = {sa, sb, sc, sd, sa};
                            libsumo::TraCIPositionVector endshape = {ea, eb, ec, ed, ea};
                            std::vector<libsumo::TraCIPositionVector> dragshape = {startshape, endshape};
                            list_dpID = {"s" + std::to_string(++this->dp_id), "s" + std::to_string(++this->dp_id)};
                            for(int i = 0; i<list_dpID.size(); i++){
                                this->polygon.add(list_dpID[i] , dragshape[i], {255, 0, 0, 255}, false, "drag", 0);
                            }
                            this->drag_wait_start = 0;

                            // drag_route = getRouteIDfromstartEdge(startEdge);
                            drag_route = genRoutestartfromCoord_Straight(startxy, endxy);
                            drag_start = true;

                            this->passedEdges = this->route.getEdges(drag_route);
                            for(int i = 0; i<this->passedEdges.size(); i++){
                                int thisEdgeNumLane = this->get_num_lane(this->passedEdges[i]);
                                std::vector<double> tempOrgSpeed;
                                for(int k = 0; k<thisEdgeNumLane; k++){
                                    tempOrgSpeed.push_back(this->lane.getMaxSpeed(this->passedEdges[i] + "_" + std::to_string(k)));

                                }
                                this->originalLaneMaxSpeed.push_back(tempOrgSpeed);
                            }
                        }
                        else if(drag_start && !drag_startgen){
                            std::string startLane = getNearestLane(this->anom_sim_para.anomaly_drag_startx, this->anom_sim_para.anomaly_drag_starty);
                            std::string startEdge = getNearestEdge(this->anom_sim_para.anomaly_drag_startx, this->anom_sim_para.anomaly_drag_starty);

                            std::string endLane = getNearestLane(this->anom_sim_para.anomaly_drag_endx, this->anom_sim_para.anomaly_drag_endy);
                            std::string endEdge = getNearestEdge(this->anom_sim_para.anomaly_drag_endx, this->anom_sim_para.anomaly_drag_endy);
                            int nLane = get_num_lane(startLane);

                            for(int k = 0; k<nLane; k++){
                            //     /**partial initialization*/
                                this->drag_id.push_back(std::to_string(++id));
                                this->drag_start_arrive.push_back(0);
                                this->drag_end_arrive.push_back(0);
                                this->drag_speed.push_back(0);
                                this->drag_accele.push_back(distaccele(genu));
                            //
                            //     // std::string to_RouteID = getRouteIDfromstartendEdge(startEdge, endEdge);
                            //     // std::string to_RouteID = getRouteIDfromstartEdge(startEdge);
                            //     int count_it = 0;
                            //     while(true){
                            //         if(to_RouteID != "-1" || count_it >= this->route_check_time){
                            //             break;
                            //         }
                            //         // to_RouteID = getRouteIDfromstartendEdge(startEdge, endEdge);
                            //         to_RouteID = getRouteIDfromstartEdge(startEdge);
                            //         count_it++;
                            //     }
                            //
                            //     if(to_RouteID != "-1"){
                                    this->vehicle.add(this->drag_id.back(), drag_route, "car", std::to_string(-1), std::to_string(k), "base", "0", std::to_string(k));
                                    this->vehicle.setColor(this->drag_id.back(), {255, 102, 255, 255}); //this is pink

                                // }
                                // else{
                                //     this->drag_id.pop_back();
                                //     this->drag_start_arrive.pop_back();
                                //     this->drag_end_arrive.pop_back();
                                // }
                            }
                            drag_startgen = true;
                        }
                        else if(drag_start && drag_startgen){
                            std::vector<std::string> vlist =  this->vehicle.getIDList();
                            for(int i = 0; i<this->drag_id.size(); i++){
                                std::vector<std::string>::iterator vit = std::find(vlist.begin(), vlist.end(), this->drag_id[i]);
                                if(vit == vlist.end()){
                                    this->drag_id.erase(this->drag_id.begin() + i);
                                    this->drag_start_arrive.erase(this->drag_start_arrive.begin() + i);
                                    this->drag_end_arrive.erase(this->drag_end_arrive.begin() + i);
                                    this->drag_speed.erase(this->drag_speed.begin() + i);
                                    this->drag_accele.erase(this->drag_accele.begin() + i);
                                }
                            }


                            for(int i = 0; i<this->drag_id.size(); i++){
                                // if(this->vehicle.getRoadID(this->drag_id[i]).find(":") == std::string::npos){
                                //     std::cout << "HERE  " <<this->drag_id[i] << "\t" << this->vehicle.getRoadID(this->drag_id[i]) + "_" + std::to_string(i) << std::endl;
                                //     this->vehicle.moveTo(this->drag_id[i], this->vehicle.getRoadID(this->drag_id[i]) + "_" + std::to_string(i), this->vehicle.getLanePosition(this->drag_id[i]));
                                // }
                                libsumo::TraCIPosition veh_pos_now = this->vehicle.getPosition(this->drag_id[i]);
                                // double veh_now_veh = this->vehicle.getSpeed(this->drag_id[i]);
                                if(this->drag_start_arrive[i] == 0){
                                    this->drag_speed[i] += this->drag_accele[i] * 0.01;
                                }

                                if(this->drag_speed[i] > this->vehicletype.getMaxSpeed("car")){
                                    this->drag_speed[i] = this->vehicletype.getMaxSpeed("car");
                                }
                                else if(this->drag_speed[i] < 0){
                                    this->drag_speed[i] = 0;
                                }
                                double veh_now_veh = this->drag_speed[i];
                                double veh_now_ang = this->vehicle.getAngle(this->drag_id[i]);

                                double x = veh_now_veh * cos((veh_now_ang - 90) / 180 * M_PI);
                                double y = veh_now_veh * sin((veh_now_ang + 90) / 180 * M_PI);
                                if(this->drag_start_arrive[i] == 0){
                                    this->vehicle.setSpeed(this->drag_id[i], 0);

                                    // this->vehicle.moveToXY(vID, eID, 0, (this->p.x+this->x) , (this->p.y+this->y), (this->da) , 2);

                                    this->vehicle.moveToXY(this->drag_id[i], "", i, veh_pos_now.x + x, veh_pos_now.y + y, INVALID_DOUBLE_VALUE, 2);
                                }






                                // this->vehicle.moveTo(this->drag_id[i], this->vehicle.getRoadID(this->drag_id[i]) + "_" + std::to_string(i), this->vehicle.getLanePosition(this->drag_id[i]));
                                // libsumo::TraCIPosition veh_pos_now = this->vehicle.getPosition(this->drag_id[i]);
                                // double veh_ang_now = this->vehicle.getAngle(this->drag_id[i]);
                                // this->vehicle.moveToXY(this->drag_id[i], "", i, veh_pos_now.x, veh_pos_now.y, veh_ang_now, 1);
                                // if(this->drag_end_arrive[i] != 1 && this->drag_start_arrive[i] != 1){
                                    /**not pass start and end*/
                                    // libsumo::TraCIPosition veh_pos_now = this->vehicle.getPosition(this->drag_id[i]);

                                    if(if_vehilce_in_area(this->drag_startend_point[0], veh_pos_now) && this->drag_start_arrive[i] == 0){
                                        this->drag_start_arrive[i] = 1;
                                        this->drag_speed[i] = 0;
                                        // this->vehicle.slowDown(this->drag_id[i], 0, 2);
                                        this->vehicle.setSpeed(this->drag_id[i], 0);
                                        std::cout << "HERE" << std::endl;
                                    }
                                // }

                                // else if(this->drag_start_arrive[i] == 1 && this->drag_end_arrive[i] != 1){
                                //     libsumo::TraCIPosition veh_pos_now = this->vehicle.getPosition(this->drag_id[i]);
                                // }
                                // else if(this->drag_start_arrive[i] == 1 && this->drag_end_arrive[i] == 1){
                                //
                                // }
                            }
                            if(calc_sum(this->drag_start_arrive) >= this->drag_id.size() && !drag_race_start){
                                /**race not start, waiting*/
                                this->drag_wait_start++;
                            }
                            if(this->drag_wait_start >= 10 && !drag_race_start && checkVehLaneClear(this->drag_id)){
                                /**finish wait time, going to start*/
                                for(int i = 0; i<this->drag_id.size(); i++){
                                    std::cout << "thiss " << std::endl;
                                    // this->vehicle.changeTarget(drag_id[i], getNearestEdge(this->anom_sim_para.anomaly_drag_endx, this->anom_sim_para.anomaly_drag_endy));

                                    // this->vehicle.setSpeed(this->drag_id[i], 10);
                                    this->vehicle.moveToXY(this->drag_id[i], "", 0, this->vehicle.getPosition(this->drag_id[i]).x, this->vehicle.getPosition(this->drag_id[i]).y, this->vehicle.getAngle(this->drag_id[i]), 1);
                                    this->vehicle.rerouteTraveltime(this->drag_id[i]);

                                    this->vehicle.setMaxSpeed(this->drag_id[i], 100 / 3.6);
                                    this->lane.setMaxSpeed(this->vehicle.getLaneID(this->drag_id[i]), 100 / 3.6);
                                    // this->vehicle.slowDown(this->drag_id[i], 100 / 3.6, distaccele_Time(genu));
                                    this->vehicle.setSpeed(this->drag_id[i], 100 / 3.6);
                                }
                                drag_race_start = true;
                            }
                            if(drag_race_start){
                                for(int i = 0; i<this->drag_id.size(); i++){
                                    if(this->drag_end_arrive[i] == 0){
                                        this->vehicle.setMaxSpeed(this->drag_id[i], 100 / 3.6);
                                        this->lane.setMaxSpeed(this->vehicle.getLaneID(this->drag_id[i]), 100 / 3.6);
                                        // this->vehicle.slowDown(this->drag_id[i], 100 / 3.6, distaccele_Time(genu));
                                        this->vehicle.setSpeed(this->drag_id[i], 100 / 3.6);
                                    }


                                    libsumo::TraCIPosition veh_pos_now = this->vehicle.getPosition(this->drag_id[i]);
                                    if(if_vehilce_in_area(this->drag_startend_point[1], veh_pos_now) && this->drag_end_arrive[i] == 0){
                                        this->drag_end_arrive[i] = 1;

                                        this->vehicle.setSpeed(this->drag_id[i], -1);


                                        std::string nEdge = getNearestEdge(this->vehicle.getPosition(this->drag_id[i]));
                                        std::string to_RouteID = getRouteIDfromstartEdge(nEdge);

                                        int count_it = 0;
                                        while(true){
                                            if(to_RouteID != "-1" || count_it >= this->route_check_time){
                                                break;
                                            }
                                            to_RouteID = getRouteIDfromstartEdge(nEdge);
                                            count_it++;
                                        }
                                        if(to_RouteID != "-1"){
                                            std::vector<std::string> all_edges = this->route.getEdges(to_RouteID);
                                            this->vehicle.changeTarget(this->drag_id[i], all_edges.back());
                                        }
                                    }
                                    else{
                                        if(this->print_file){
                                            label_one(this->file_label, "drag", this->drag_id[i]);
                                        }
                                        if(this->print_file_instance){
                                            label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "drag", this->drag_id[i]);
                                        }

                                    }
                                }
                                if(calc_sum(this->drag_end_arrive) >= this->drag_id.size()){
                                    this->drag_id.clear();
                                    this->drag_start_arrive.clear();
                                    this->drag_end_arrive.clear();
                                    this->drag_speed.clear();
                                    this->drag_accele.clear();

                                    this->drag_wait_start = 0;
                                    drag_start = false;
                                    drag_startgen = false;
                                    drag_race_start = false;

                                    for(int i = 0; i<this->passedEdges.size(); i++){
                                        int thisEdgeNumLane = get_num_lane(this->passedEdges[i]);
                                        for(int k = 0; k<thisEdgeNumLane; k++){
                                            this->lane.setMaxSpeed(this->passedEdges[i] + "_" + std::to_string(k), this->originalLaneMaxSpeed[i][k]);
                                        }
                                    }
                                    this->originalLaneMaxSpeed.clear();
                                    this->passedEdges.clear();

                                    for(int i = 0; i<list_dpID.size(); i++){
                                        this->polygon.remove(list_dpID[i]);
                                    }

                                }

                            }
                        }
                    }
                    else if(a_type == g_coord_motion && distd(genu) <= this->anom_sim_para.anomaly_coord_motion || group_coord_start || group_coord_startgen){


                        if(!group_coord_start && !group_coord_startgen){
                            area tempa;
                            // tempa.minx = distgx(genu);
                            // tempa.miny = distgy(genu);
                            std::string at_lane;
                            libsumo::TraCIPosition the_point = randomPointOnLane(at_lane);
                            int nLane = get_num_lane(at_lane);
                            tempa.minx = the_point.x - (nLane + 1) * this->averageLaneWidth;
                            tempa.miny = the_point.y - (nLane + 1)* this->averageLaneWidth;
                            tempa.xlen = this->averageLaneWidth * (nLane + 5);
                            tempa.ylen = this->averageLaneWidth * (nLane + 5);
                            tempa.maxx = tempa.minx + tempa.xlen;
                            tempa.maxy = tempa.miny + tempa.ylen;
                            this->group_coord_meet_point.push_back(tempa);
                            group_coord_start = true;
                            this->group_coord_waittime_count.push_back(0);
                            // std::cout << this->group_coord_meet_point.size() << std::endl;
                            std::cout << "stage 1"<< std::endl;

                            libsumo::TraCIPosition a, b, c, d;
                            a.x = tempa.minx;
                            a.y = tempa.miny;
                            b.x = tempa.minx;
                            b.y = tempa.maxy;
                            c.x = tempa.maxx;
                            c.y = tempa.maxy;
                            d.x = tempa.maxx;
                            d.y = tempa.miny;

                            libsumo::TraCIPositionVector meetshape = {a, b, c, d, a};
                            list_pID.push_back("m" + std::to_string(++this->p_id));
                            this->polygon.add(list_pID.back() , meetshape, {255, 0, 0, 255}, false, "meet", 0);
                            // std::cout << "poly add" << std::endl;
                        }
                        else if(group_coord_start && !group_coord_startgen){
                            std::cout << "stage 2"<< std::endl;
                            // std::cout << this->group_coord_meet_point.size() << std::endl;
                            for(int g = 0; g<this->group_coord_meet_point.size(); g++){
                                std::vector<int> temp_arrive;
                                int num = distcoord(genu);
                                // std::cout << num << std::endl;
                                // num = 2;
                                std::vector<std::string> total_route, temp_coord_id;
                                std::string nEdge = getNearestEdge(this->group_coord_meet_point[g].minx, this->group_coord_meet_point[g].miny);
                                for(int i = 0; i<num; i++){
                                    temp_arrive.push_back(0);
                                    temp_coord_id.push_back(std::to_string(++id));
                                    // std::cout << num << "\t" << temp_coord_id.back() << std::endl;
                                    // std::string temp_route = std::to_string(distu(genu));
                                    // // std::vector<std::string>::iterator rit = std::find(total_route.begin(), total_route.end(), temp_route);
                                    // // if(rit == total_route.end()){
                                    // //     temp_route = std::to_string(distu(genu));
                                    // //     rit = std::find(total_route.begin(), total_route.end(), temp_route);
                                    // // }
                                    // total_route.push_back(temp_route);
                                    //
                                    // this->vehicle.add(temp_coord_id.back(), total_route.back(), "car", std::to_string(-1));
                                    // this->vehicle.changeTarget(temp_coord_id.back(), getNearestEdge(this->group_coord_meet_point[g].minx, this->group_coord_meet_point[g].miny));
                                    // this->vehicle.setColor(temp_coord_id.back(), {255, 153, 51, 255});


                                    std::string to_RouteID = getRouteIDfromendEdge(nEdge);

                                    int count_it = 0;
                                    while(true){
                                        if(to_RouteID != "-1" || count_it >= this->route_check_time){
                                            break;
                                        }
                                        to_RouteID = getRouteIDfromendEdge(nEdge);
                                        count_it++;
                                    }
                                    if(to_RouteID != "-1"){
                                        this->vehicle.add(temp_coord_id.back(), to_RouteID, "car", std::to_string(-1));
                                        this->vehicle.setColor(temp_coord_id.back(), {255, 153, 51, 255}); //this is orange
                                    }
                                    else{
                                        temp_coord_id.pop_back();
                                        temp_arrive.pop_back();
                                    }
                                }
                                this->coord_id.push_back(temp_coord_id);
                                this->group_coord_arrive.push_back(temp_arrive);
                            }
                            group_coord_startgen = true;
                        }
                        else if(group_coord_start && group_coord_startgen){
                            /**before everything, check exist*/
                            for(int g = 0; g<this->coord_id.size(); g++){
                                for(int i = 0; i<this->coord_id[g].size(); i++){
                                    std::vector<std::string> vehicle_list = this->vehicle.getIDList();
                                    std::vector<std::string>::iterator vit = std::find(vehicle_list.begin(), vehicle_list.end(), this->coord_id[g][i]);
                                    if(vit == vehicle_list.end()){
                                        /**don't exist*/
                                        this->coord_id[g].erase(this->coord_id[g].begin() + i);
                                        this->group_coord_arrive[g].erase(this->group_coord_arrive[g].begin() + i);
                                    }
                                }
                                if(this->coord_id[g].size() == 0){
                                    this->group_coord_waittime_count.erase(this->group_coord_waittime_count.begin() + g);
                                    this->coord_id.erase(this->coord_id.begin() + g);
                                    this->group_coord_meet_point.erase(this->group_coord_meet_point.begin() + g);
                                    this->group_coord_arrive.erase(this->group_coord_arrive.begin() + g);

                                    this->polygon.remove(list_pID[g]);
                                    // std::cout << "poly remove" << std::endl;
                                    list_pID.erase(list_pID.begin() + g);
                                }
                            }
                            std::cout << "stage 3"<< std::endl;
                            for(int g = 0; g<this->coord_id.size(); g++){
                                if(this->coord_id.size() == 0){
                                    group_coord_start = false;
                                    group_coord_startgen = false;
                                }
                                double total_arrived = 0;
                                for(int i = 0; i<this->coord_id[g].size(); i++){
                                    // libsumo::TraCIPosition veh_pos = this->vehicle.getPosition(this->coord_id[g][i]);
                                    // std::vector<std::string> thisroute = this->vehicle.getRoute(this->coord_id[g][i]);
                                    //
                                    // std::cout << "what " << this->group_coord_meet_point[g].minx << " " <<veh_pos.x <<std::endl;
                                    // std::cout <<getNearestEdge(this->group_coord_meet_point[g].minx, this->group_coord_meet_point[g].miny) << " " << thisroute.back() <<std::endl;
                                    // if(getNearestEdge(this->vehicle.getPosition(this->coord_id[g][i])) == getNearestEdge(this->group_coord_meet_point[g].minx, this->group_coord_meet_point[g].miny)){
                                    // if(this->lane.getEdgeID(this->vehicle.getLaneID(this->coord_id[g][i])) ==  getNearestEdge(this->group_coord_meet_point[g].minx, this->group_coord_meet_point[g].miny)){
                                        libsumo::TraCIPosition veh_pos = this->vehicle.getPosition(this->coord_id[g][i]);
                                        if(this->print_file){
                                            label_one(this->file_label, "coord", this->coord_id[g][i]);
                                        }
                                        if(this->print_file_instance){
                                            label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "coord", this->coord_id[g][i]);
                                        }
                                        if(this->if_vehilce_in_area(this->group_coord_meet_point[g], veh_pos) && group_coord_arrive[g][i] == 0){
                                            /** this is within*/
                                            std::cout << " with in " <<std::endl;
                                            // this->vehicle.setSpeed(this->coord_id[g][i], 0);


                                            std::string nEdge = getNearestEdge(this->vehicle.getPosition(this->coord_id[g][i]));
                                            std::string to_RouteID = getRouteIDfromstartEdge(nEdge);

                                            int count_it = 0;
                                            while(true){
                                                if(to_RouteID != "-1" || count_it >= this->route_check_time){
                                                    break;
                                                }
                                                to_RouteID = getRouteIDfromstartEdge(nEdge);
                                                count_it++;
                                            }
                                            if(to_RouteID != "-1"){
                                                // this->vehicle.setRouteID(this->coord_id[g][i], to_RouteID);

                                                std::vector<std::string> all_edges = this->route.getEdges(to_RouteID);
                                                this->vehicle.changeTarget(this->coord_id[g][i], all_edges.back());
                                            }
                                            this->vehicle.slowDown(this->coord_id[g][i], 0, 5);

                                            this->group_coord_arrive[g][i] = 1;
                                            total_arrived++;
                                            /**count coord id passed here *///////////////////////////
                                        }
                                        else if(!this->if_vehilce_in_area(this->group_coord_meet_point[g], veh_pos) && group_coord_arrive[g][i] == 1){
                                            this->vehicle.setSpeed(this->coord_id[g][i], 0);
                                        }
                                    // }
                                }
                                std::cout << "stage 3-1" << std::endl;
                                // if(total_arrived >= this->coord_id[g].size()){
                                //     this->group_coord_waittime_count[g]++;
                                // }
                                if(calc_sum(this->group_coord_arrive[g]) >= this->coord_id[g].size() && this->coord_id[g].size() != 0){
                                    this->group_coord_waittime_count[g]++;
                                }
                                std::cout << "stage 3-2" << std::endl;
                                if(this->group_coord_waittime_count[g] >= this->anom_sim_para.group_wait_time){
                                    this->polygon.remove(list_pID[g]);
                                    // std::cout << "poly remove" << std::endl;
                                    list_pID.erase(list_pID.begin() + g);

                                    for(int i = 0; i<this->coord_id[g].size(); i++){
                                        // double distribute_x = distgx(genu);
                                        // double distribute_y = distgy(genu);
                                        // std::vector<std::string> alledges = this->edge.getIDList();
                                        // std::uniform_int_distribution<int> distedge(0, alledges.size() - 1);
                                        // std::string temp_edge = alledges[distedge(genu)];
                                        // // this->vehicle.changeTarget(this->coord_id[g][i], getNearestEdge(distribute_x, distribute_y));
                                        // while(temp_edge.find(":") != std::string::npos){
                                        //     temp_edge = alledges[distedge(genu)];
                                        // }
                                        // this->vehicle.changeTarget(this->coord_id[g][i], temp_edge);

                                        /**maybe need to set speed back to max speed*///////////////////////////////
                                        this->vehicle.setSpeed(this->coord_id[g][i], -1);
                                    }
                                    this->group_coord_waittime_count.erase(this->group_coord_waittime_count.begin() + g);
                                    this->coord_id.erase(this->coord_id.begin() + g);
                                    this->group_coord_meet_point.erase(this->group_coord_meet_point.begin() + g);
                                    this->group_coord_arrive.erase(this->group_coord_arrive.begin() + g);
                                    group_coord_start = false;
                                    group_coord_startgen = false;
                                }
                            }
                        }

                    }



                    /** group army follow inline*/
                    int countEmptyLane = 0;
                    for(int l = 0; l<group_army_id.size(); l++){
                        if(group_army_id[l].size() == 0){
                            countEmptyLane++;

                        }
                        for(int i = 0; i<group_army_id[l].size(); i++){
                            std::vector<std::string> v_list = this->vehicle.getIDList();
                            std::vector<std::string>::iterator vit = std::find(v_list.begin(), v_list.end(), group_army_id[l][i]);
                            if(vit != v_list.end() && !group_army_id_added[l][i]){
                                group_army_id_added[l][i] = true;
                            }
                            else if(vit != v_list.end() && group_army_id_added[l][i]){

                                vehicle_para leading_vehcile_now;
                                vehicle_para vehicle_now;
                                this->ap = this->vehicle.getPosition(group_army_id[l][i]);
                                leading_vehcile_now.x = this->ap.x;
                                leading_vehcile_now.y = this->ap.y;
                                this->aa = this->vehicle.getAngle(group_army_id[l][i]);
                                leading_vehcile_now.ang = this->aa;

                                group_army_inline_para[l].push_back(leading_vehcile_now);
                                if(this->print_file){
                                    label_one(this->file_label, "gp_arm", group_army_id[l][i]);
                                }
                                if(this->print_file_instance){
                                    label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "gp_arm", group_army_id[l][i]);
                                }
                                if(i > 0 && abs(log10(group_army_inline_para[l][i - 1].x)) < 3 && abs(log10(group_army_inline_para[l][i - 1].y)) < 3){
                                    this->vehicle.moveToXY(group_army_id[l][i], "", 0, group_army_inline_para[l][i - 1].x, group_army_inline_para[l][i - 1].y, group_army_inline_para[l][i - 1].ang, 2);
                                }
                            }
                            else if(vit == v_list.end() && group_army_id_added[l][i] && !group_army_id_then_removed[l][i]){
                                // if(num_army <= 0){
                                    group_army_id[l].erase(group_army_id[l].begin() + i);
                                    group_army_inline_para[l].erase(group_army_inline_para[l].begin() + i - 1);
                                    group_army_id_added[l].erase(group_army_id_added[l].begin() + i);
                                    group_army_id_then_removed[l].erase(group_army_id_then_removed[l].begin() + i);
                                    if(i + 1 < group_army_id[l].size() && abs(log10(group_army_inline_para[l][i - 1].x)) < 3 && abs(log10(group_army_inline_para[l][i - 1].y)) < 3){
                                        this->vehicle.moveToXY(group_army_id[l][i], "", 0, group_army_inline_para[l][i - 1].x, group_army_inline_para[l][i - 1].y, group_army_inline_para[l][i - 1].ang, 1);
                                    }

                                // }

                            }
                            if(group_army_id[l].size() <= 0){
                                group_army_inline_para[l].clear();
                                group_army_id_added[l].clear();
                                group_army_id_then_removed[l].clear();
                            }
                        }

                    }
                    if(countEmptyLane == group_army_id.size()){
                        group_army_id.clear();
                        group_army_inline_para.clear();
                        group_army_id_added.clear();
                        group_army_id_then_removed.clear();
                        group_army_start = false;
                        num_army = distarmy(genu);

                    }

                }
                if(this->user_vehicle){
                    if(!this->del && this->added){
                        this->x = this->speed * cos((-da + 90) / 180 * M_PI);
                        this->y = this->speed * sin((-da + 90) / 180 * M_PI);

                        this->vehicle.setSpeed(vID, this->speed);
                        this->vehicle.moveToXY(vID, eID, 0, (this->p.x+this->x) , (this->p.y+this->y), (this->da) , 2);

                        //this->vehicle.moveTo(vID, this->vehicle.getRoadID(vID) + "_" + std::to_string(2), this->vehicle.getLanePosition(vID));
                        if(this->print_file_user){
                            label_user(this->file_label_user, this->simulation.getCurrentTime(), this->vehicle.getPosition(vID), this->vehicle.getAngle(vID), vID);
                        }
                        // std::cout << "l " << this->vehicle.getLaneID(vID) << std::endl;
                        // std::cout << "r " << this->vehicle.getRoadID(vID) << std::endl;
                        // std::cout << "e " <<getNearestEdge(this->vehicle.getPosition(vID)) << std::endl;
                    }
                    if(this->del && this->added){
                        this->vehicle.remove(vID);
                        std::cout << "User Vehicle Removed: " << vID << std::endl;
                        this->added = false;
                    }
                    if(this->del && this->add && !this->added){
                        //while(getch() != 102){};
                        this->strv = "R" + std::to_string(++r_id);
                        this->vehicle.add(this->strv, std::to_string(distu(genu)), "car",  std::to_string(-1));
                        this->vehicle.setColor(this->strv, {255, 0, 0, 255});
                        std::cout << "User Vehicle Added: " << this->strv << std::endl;
                        this->del = false;
                        this->added = true;
                        this->add = false;
                        this->flag = false;
                    }
                    if(this->added && this->add){
                        std::cout << "Can Only Exist One User Vehicle" << std::endl;
                        this->add = false;
                    }

                }


            }
//            std::cout << "one" << this->lane.getIDCount() <<std::endl;
            // double odd_event_perc = distd(genu);
            std::vector<std::string> v_list = this->vehicle.getIDList();
            std::vector<std::string>::iterator vit = std::find(v_list.begin(), v_list.end(), std::to_string(id));
            if(vit != v_list.end()){
                /**lane changes*/
                std::vector<std::string>::iterator fit = std::find(fast_lane_list.begin(), fast_lane_list.end(), std::to_string(id));
                std::vector<std::string>::iterator uit = std::find(bad_uturn_list.begin(), bad_uturn_list.end(), std::to_string(id));
                std::vector<std::string>::iterator spit = std::find(speed_list.begin(), speed_list.end(), std::to_string(id));
                std::vector<std::string>::iterator sit = std::find(slow_list.begin(), slow_list.end(), std::to_string(id));
                std::vector<std::string>::iterator ssit = std::find(speed_slow_list.begin(), speed_slow_list.end(), std::to_string(id));
                std::vector<std::string>::iterator osit = std::find(off_str_list.begin(), off_str_list.end(), std::to_string(id));
                std::vector<std::string>::iterator opit = std::find(off_per_list.begin(), off_per_list.end(), std::to_string(id));
                if(odd_event_perc < fast_lane_perc && c % freq == 0 && c != 0 && id > 0 && fit == fast_lane_list.end() && this->vehicle.getTypeID(std::to_string(id)) == "car"){
                    fast_lane_list.push_back(std::to_string(id));
                    int lane_index_now = this->vehicle.getLaneIndex(std::to_string(id));
                    fast_lane_plane.push_back(lane_index_now);
                }
                /**bad uturn*/
                else if(odd_event_perc < bad_uturn_perc + fast_lane_perc && odd_event_perc >= fast_lane_perc && c % freq == 0 && c != 0 && id > 0 && this->vehicle.getTypeID(std::to_string(id)) == "car"
                        && uit == bad_uturn_list.end() && spit == speed_list.end() && sit == slow_list.end() && ssit == speed_slow_list.end()){
                    bad_uturn_list.push_back(std::to_string(id));
                    std::uniform_real_distribution<double> distp(this->lane_length * uturn_pos_perc, this->lane_length * (1 - uturn_pos_perc));

                    bad_uturn_on_lane.push_back(distb(genu));
                    bad_uturn_at_pos.push_back(distp(genu));
                    bad_uturn_veh_atlane_now.push_back(-1);
                    bad_uturn_state.push_back(0);
                    bad_uturn_progress.push_back(false);
                }
                /**speeding or too slow*/
//                std::vector<std::string>::iterator uit = std::find(bad_uturn_list.begin(), bad_uturn_list.end(), std::to_string(id));
    //            double speed_event_perc = distd(genu);
                if(odd_event_perc < speeding_slow_perc + bad_uturn_perc + fast_lane_perc && odd_event_perc >= bad_uturn_perc + fast_lane_perc && c % freq == 0 && c != 0 && id > 0 && uit == bad_uturn_list.end() && this->vehicle.getTypeID(std::to_string(id)) == "car"){
                    double speed_event_choice = distd(genu);
                    if(speed_event_choice <= this->anom_sim_para.anomaly_speed_sp && spit == speed_list.end()){
                        speed_list.push_back(std::to_string(id));
                        speed_list_speed.push_back(distd(genu) * 5 + 5);
                    }
                    else if(this->anom_sim_para.anomaly_speed_sp < speed_event_choice && speed_event_choice <= this->anom_sim_para.anomaly_speed_sp + this->anom_sim_para.anomaly_speed_slow && sit == slow_list.end()){
                        slow_list.push_back(std::to_string(id));
                        slow_list_slow.push_back(distd(genu) * 5 + 5);
                    }
                    else if(speed_event_choice > 1 - this->anom_sim_para.anomaly_speed_spsl && ssit == speed_slow_list.end()){
                        speed_slow_list.push_back(std::to_string(id));
                    }
                }
                /**off road*/
                if(odd_event_perc < speeding_slow_perc + bad_uturn_perc + fast_lane_perc + off_road_perc && odd_event_perc >= speeding_slow_perc + bad_uturn_perc + fast_lane_perc && c % freq == 0 && c != 0 && id > 0 && uit == bad_uturn_list.end()
                    && this->vehicle.getTypeID(std::to_string(id)) == "car" && spit == speed_list.end() && sit == slow_list.end() && ssit == speed_slow_list.end() && fit == fast_lane_list.end() ){
                    double off_choice = distd(genu);
                    if(1 <= off_choice && osit == off_str_list.end() && opit == off_per_list.end()){
                        off_str_list.push_back(std::to_string(id));
                        off_str_progress.push_back(false);
                        off_str_state.push_back(0);
                    }
                    else if(off_choice < 1 && opit == off_per_list.end() && osit == off_str_list.end()){
                        off_per_list.push_back(std::to_string(id));
                        off_per_progress.push_back(false);
                        off_per_state.push_back(0);
                    }
                }
            }

            /**if vehicle enter this area, increase prob to be anomaly*/
            double inc_odd_event_perc = odd_event_perc + increase_anom_prob;

            for(int i = 0; i<this->area_id_list.size(); i++){
                int area_id = std::stoi(area_id_list[i], &sz);
                std::vector<std::string>::iterator nvit = std::find(v_list.begin(), v_list.end(), std::to_string(area_id));
                if(nvit != v_list.end()){
                    /**lane changes*/
                    std::vector<std::string>::iterator fit = std::find(fast_lane_list.begin(), fast_lane_list.end(), std::to_string(area_id));
                    std::vector<std::string>::iterator uit = std::find(bad_uturn_list.begin(), bad_uturn_list.end(), std::to_string(area_id));
                    std::vector<std::string>::iterator spit = std::find(speed_list.begin(), speed_list.end(), std::to_string(area_id));
                    std::vector<std::string>::iterator sit = std::find(slow_list.begin(), slow_list.end(), std::to_string(area_id));
                    std::vector<std::string>::iterator ssit = std::find(speed_slow_list.begin(), speed_slow_list.end(), std::to_string(area_id));
                    std::vector<std::string>::iterator osit = std::find(off_str_list.begin(), off_str_list.end(), std::to_string(area_id));
                    std::vector<std::string>::iterator opit = std::find(off_per_list.begin(), off_per_list.end(), std::to_string(area_id));
                    if(inc_odd_event_perc < fast_lane_perc && c % freq == 0 && c != 0 && area_id > 0 && fit == fast_lane_list.end() && this->vehicle.getTypeID(std::to_string(area_id)) == "car"){
                        fast_lane_list.push_back(std::to_string(area_id));
                        int lane_index_now = this->vehicle.getLaneIndex(std::to_string(area_id));
                        fast_lane_plane.push_back(lane_index_now);
                    }
                    /**bad uturn*/
                    else if(inc_odd_event_perc < bad_uturn_perc + fast_lane_perc && inc_odd_event_perc >= fast_lane_perc && c % freq == 0 && c != 0 && area_id > 0 && this->vehicle.getTypeID(std::to_string(area_id)) == "car"
                            && uit == bad_uturn_list.end() && spit == speed_list.end() && sit == slow_list.end() && ssit == speed_slow_list.end()){
                        bad_uturn_list.push_back(std::to_string(area_id));
                        std::uniform_real_distribution<double> distp(this->lane_length * uturn_pos_perc, this->lane_length * (1 - uturn_pos_perc));

                        bad_uturn_on_lane.push_back(distb(genu));
                        bad_uturn_at_pos.push_back(distp(genu));
                        bad_uturn_veh_atlane_now.push_back(-1);
                        bad_uturn_state.push_back(0);
                        bad_uturn_progress.push_back(false);
                    }
                    /**speeding or too slow*/
    //                std::vector<std::string>::iterator uit = std::find(bad_uturn_list.begin(), bad_uturn_list.end(), std::to_string(area_id));
        //            double speed_event_perc = distd(genu);
                    if(inc_odd_event_perc < speeding_slow_perc + bad_uturn_perc + fast_lane_perc && inc_odd_event_perc >= bad_uturn_perc + fast_lane_perc && c % freq == 0 && c != 0 && area_id > 0 && uit == bad_uturn_list.end() && this->vehicle.getTypeID(std::to_string(area_id)) == "car"){
                        double speed_event_choice = distd(genu);
                        if(speed_event_choice <= this->anom_sim_para.anomaly_speed_sp && spit == speed_list.end()){
                            speed_list.push_back(std::to_string(area_id));
                            speed_list_speed.push_back(distd(genu) * 5 + 5);
                        }
                        else if(this->anom_sim_para.anomaly_speed_sp < speed_event_choice && speed_event_choice <= this->anom_sim_para.anomaly_speed_sp + this->anom_sim_para.anomaly_speed_slow && sit == slow_list.end()){
                            slow_list.push_back(std::to_string(area_id));
                            slow_list_slow.push_back(distd(genu) * 5 + 5);
                        }
                        else if(speed_event_choice > 1 - this->anom_sim_para.anomaly_speed_spsl && ssit == speed_slow_list.end()){
                            speed_slow_list.push_back(std::to_string(area_id));
                        }
                    }
                    /**off road*/
                    if(inc_odd_event_perc < speeding_slow_perc + bad_uturn_perc + fast_lane_perc + off_road_perc && inc_odd_event_perc >= speeding_slow_perc + bad_uturn_perc + fast_lane_perc && c % freq == 0 && c != 0 && area_id > 0 && uit == bad_uturn_list.end()
                        && this->vehicle.getTypeID(std::to_string(area_id)) == "car" && spit == speed_list.end() && sit == slow_list.end() && ssit == speed_slow_list.end() && fit == fast_lane_list.end() ){
                        double off_choice = distd(genu);
                        if(1 <= off_choice && osit == off_str_list.end() && opit == off_per_list.end()){
                            off_str_list.push_back(std::to_string(area_id));
                            off_str_progress.push_back(false);
                            off_str_state.push_back(0);
                        }
                        else if(off_choice < 1 && opit == off_per_list.end() && osit == off_str_list.end()){
                            off_per_list.push_back(std::to_string(area_id));
                            off_per_progress.push_back(false);
                            off_per_state.push_back(0);
                        }
                    }
                }

            }
            /** if vehicle exit this area, become normal*/
            if(this->past_area_id_list.size() != 0){
                for(int i = 0; i<this->past_area_id_list.size(); i++){
                    std::vector<std::string>::iterator area_it = std::find(this->area_id_list.begin(), this->area_id_list.end(), this->past_area_id_list[i]);
                    if(area_it != this->area_id_list.end()){
                        std::string found_id = *area_it;

                        std::vector<std::string>::iterator fit = std::find(fast_lane_list.begin(), fast_lane_list.end(), found_id);
                        std::vector<std::string>::iterator uit = std::find(bad_uturn_list.begin(), bad_uturn_list.end(), found_id);
                        std::vector<std::string>::iterator spit = std::find(speed_list.begin(), speed_list.end(), found_id);
                        std::vector<std::string>::iterator sit = std::find(slow_list.begin(), slow_list.end(), found_id);
                        std::vector<std::string>::iterator ssit = std::find(speed_slow_list.begin(), speed_slow_list.end(), found_id);
                        std::vector<std::string>::iterator osit = std::find(off_str_list.begin(), off_str_list.end(), found_id);
                        std::vector<std::string>::iterator opit = std::find(off_per_list.begin(), off_per_list.end(), found_id);

                        if(fit != fast_lane_list.end()){
                            int fast_at = fit - fast_lane_list.begin();
                            fast_lane_list.erase(fast_lane_list.begin() + fast_at);
                            fast_lane_plane.erase(fast_lane_plane.begin() + fast_at);
                        }
                        if(uit != bad_uturn_list.end()){
                            int uturn_at = uit - bad_uturn_list.begin();
                            bad_uturn_list.erase(bad_uturn_list.begin() + uturn_at);
                            bad_uturn_on_lane.erase(bad_uturn_on_lane.begin() + uturn_at);
                            bad_uturn_at_pos.erase(bad_uturn_at_pos.begin() + uturn_at);
                            bad_uturn_veh_atlane_now.erase(bad_uturn_veh_atlane_now.begin() + uturn_at);
                            bad_uturn_state.erase(bad_uturn_state.begin() + uturn_at);
                            bad_uturn_progress.erase(bad_uturn_progress.begin() + uturn_at);
                        }
                        if(spit != speed_list.end()){
                            int sp_at = spit - speed_list.begin();
                            speed_list.erase(speed_list.begin() + sp_at);
                            speed_list_speed.erase(speed_list_speed.begin() + sp_at);
                        }
                        if(sit != slow_list.end()){
                            int slow_at = sit - slow_list.begin();
                            slow_list.erase(slow_list.begin() + slow_at);
                            slow_list_slow.erase(slow_list_slow.begin() + slow_at);
                        }
                        if(ssit != speed_slow_list.end()){
                            int ss_at = ssit - speed_slow_list.begin();
                            speed_slow_list.erase(speed_slow_list.begin() + ss_at);
                        }
                        if(osit != off_str_list.end()){
                            int os_at = osit - off_str_list.begin();
                            off_str_list.erase(off_str_list.begin() + os_at);
                            off_str_progress.erase(off_str_progress.begin() + os_at);
                            off_str_state.erase(off_str_state.begin() + os_at);
                        }
                        if(opit != off_per_list.end()){
                            int op_at = opit - off_per_list.begin();
                            off_per_list.erase(off_per_list.begin() + op_at);
                            off_per_progress.erase(off_per_progress.begin() + op_at);
                            off_per_state.erase(off_per_state.begin() + op_at);
                        }

                    }
                }
            }






            /**lane change algorithm*/
            for(int i = 0; i<fast_lane_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), fast_lane_list[i]);
                if(it != list.end()){
                    if(this->get_num_lane(this->vehicle.getLaneID(fast_lane_list[i])) > 1){
                        int lane_index_now = this->vehicle.getLaneIndex(fast_lane_list[i]);
                        int numoflanes = this->get_num_lane(this->vehicle.getLaneID(fast_lane_list[i]));
                        bool changed = false;
                        if(fast_lane_plane[i] != lane_index_now){
                        	changed = true;
                        }
                        fast_lane_plane[i] = lane_index_now;

                        if(lane_index_now == numoflanes - 1){
                            lane_index_now--;
                        }
                        else if(lane_index_now == 0){
                            lane_index_now++;
                        }
                        else{
                            lane_index_now += 2 * distb(genu) - 1;
                        }
                        /**if no at junction (i.e intersection)*/
                        //&& this->vehicle.getRouteID("2").find("u") == std::string::npos
                        //|| (this->vehicle.getLanePosition(fast_lane_list[i]) / this->lane.getLength(this->vehicle.getLaneID(fast_lane_list[i]))) <= 0.7
                        /**not at junction, not close to junction, with freq, with car, above speed*/
                        if((this->vehicle.getRoadID(fast_lane_list[i]).find(":") == std::string::npos && (this->vehicle.getLanePosition(fast_lane_list[i]) / this->lane.getLength(this->vehicle.getLaneID(fast_lane_list[i])) < 0.7))
                            && c % 50 == 0 && this->vehicle.getTypeID(fast_lane_list[i]) == "car" && this->vehicle.getSpeed(fast_lane_list[i]) > 2){
                            this->vehicle.moveTo(fast_lane_list[i], this->vehicle.getRoadID(fast_lane_list[i]) + "_" + std::to_string(lane_index_now), this->vehicle.getLanePosition(fast_lane_list[i]));
                            if(this->print_file){
                                label_one(this->file_label, "lane", fast_lane_list[i]);
                            }
                        }

                        if(changed){
                            if(this->print_file_instance){
                        	   label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "lane", fast_lane_list[i]);
                            }
                        }
                    }

                }
                else{
                	std::vector<std::string>::iterator fit = std::find(fast_lane_list.begin(), fast_lane_list.end(), fast_lane_list[i]);
                	int fit_at = fit - fast_lane_list.begin();
                    fast_lane_list.erase(std::remove(fast_lane_list.begin(), fast_lane_list.end(), fast_lane_list[i]), fast_lane_list.end());
                    fast_lane_plane.erase(fast_lane_plane.begin() + fit_at);
                }
            }
            /**bad uturn algorithm*/
            for(int i = 0; i<bad_uturn_list.size(); i++){
				bool rm = false;
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), bad_uturn_list[i]);
                /**exist, on lane 2, at uturn lane pos, at right lane*/
                /**do uturn*/

				if(it != list.end()){
					if(it != list.end() && this->vehicle.getLaneIndex(bad_uturn_list[i]) == this->get_num_lane(this->vehicle.getLaneID(bad_uturn_list[i])) - 1 && bad_uturn_state[i] >= 0){
		                double veh_pos_now = this->vehicle.getLanePosition(bad_uturn_list[i]);
		                if(bad_uturn_at_pos[i] >= (veh_pos_now - this->lane.getLength(this->vehicle.getLaneID(bad_uturn_list[i])) * 0.05) && bad_uturn_at_pos[i] <= (veh_pos_now + this->lane.getLength(this->vehicle.getLaneID(bad_uturn_list[i])) * 0.05)){
		                    bad_uturn_veh_atlane_now[i]++;
		                    if(bad_uturn_on_lane[i] == bad_uturn_veh_atlane_now[i]){
		                        bad_uturn_progress[i] = true;
		                    }
		                }

		            }
		            /**exist, not on lane 2, at uturn lane pos, at right lane*/
		            /**cannot uturn*/
		            else if(it != list.end() && this->vehicle.getLaneIndex(bad_uturn_list[i]) != this->get_num_lane(this->vehicle.getLaneID(bad_uturn_list[i])) - 1 && !bad_uturn_progress[i]){
		                double veh_pos_now = this->vehicle.getLanePosition(bad_uturn_list[i]);
		                if(bad_uturn_at_pos[i] >= (veh_pos_now - this->lane.getLength(this->vehicle.getLaneID(bad_uturn_list[i])) * 0.05) && bad_uturn_at_pos[i] <= (veh_pos_now + this->lane.getLength(this->vehicle.getLaneID(bad_uturn_list[i])) * 0.05)){
		                    bad_uturn_veh_atlane_now[i]++;
		                    if(bad_uturn_on_lane[i] == bad_uturn_veh_atlane_now[i]){
		                        bad_uturn_list.erase(bad_uturn_list.begin() + i);
		                        bad_uturn_on_lane.erase(bad_uturn_on_lane.begin() + i);
		                        bad_uturn_at_pos.erase(bad_uturn_at_pos.begin() + i);
		                        bad_uturn_veh_atlane_now.erase(bad_uturn_veh_atlane_now.begin() + i);
		                        bad_uturn_state.erase(bad_uturn_state.begin() + i);
		                        bad_uturn_progress.erase(bad_uturn_progress.begin() + i);

								rm = true;
		                    }
		                }

		            }
		            else if(it == list.end()){ /**not exist*/
		                std::vector<std::string>::iterator bit = std::find(bad_uturn_list.begin(), bad_uturn_list.end(), bad_uturn_list[i]);
		                int bindex = bit - bad_uturn_list.begin();
		                bad_uturn_list.erase(std::remove(bad_uturn_list.begin(), bad_uturn_list.end(), bad_uturn_list[i]), bad_uturn_list.end());

		                bad_uturn_on_lane.erase(bad_uturn_on_lane.begin() + bindex);
		                bad_uturn_at_pos.erase(bad_uturn_at_pos.begin() + bindex);
		                bad_uturn_veh_atlane_now.erase(bad_uturn_veh_atlane_now.begin() + bindex);
		                bad_uturn_state.erase(bad_uturn_state.begin() + bindex);
		                bad_uturn_progress.erase(bad_uturn_progress.begin() + bindex);

						rm = true;
		            }

					if(rm == false){
						if(bad_uturn_progress[i] && (bad_uturn_state[i] < this->uturn_speed.size()) && bad_uturn_state[i] >= 0 && it != list.end() && this->vehicle.getTypeID(bad_uturn_list[i]) == "car"){
				            double uturn_angle = this->vehicle.getAngle(bad_uturn_list[i]);
				            uturn_angle += this->uturn_angle_diff[bad_uturn_state[i]];

				            double uturn_x = (this->uturn_speed[bad_uturn_state[i]] + this->uturn_sp_diff[bad_uturn_state[i]]) / this->uturn_speed.size() * cos((uturn_angle - 90) / 180 * M_PI);
				            double uturn_y = (this->uturn_speed[bad_uturn_state[i]] + this->uturn_sp_diff[bad_uturn_state[i]]) / this->uturn_speed.size() * sin((uturn_angle + 90) / 180 * M_PI);
		//                    this->vehicle.setSpeed(bad_uturn_list[i], this->uturn_speed[bad_uturn_state[i]] + this->uturn_sp_diff[bad_uturn_state[i]] );
				            this->vehicle.moveToXY(bad_uturn_list[i], "", 0, (this->vehicle.getPosition(bad_uturn_list[i]).x + uturn_x) , (this->vehicle.getPosition(bad_uturn_list[i]).y + uturn_y), uturn_angle , 2);
				            bad_uturn_state[i]++;
                            if(this->print_file){
				                label_one(this->file_label, "buturn", bad_uturn_list[i]);
                            }
                            if(this->print_file_instance){
				                label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "buturn", bad_uturn_list[i]);
                            }
				        }
				        else if(bad_uturn_state[i] >= this->uturn_speed.size() && this->vehicle.getTypeID(bad_uturn_list[i]) == "car" && it != list.end() ){
				            bad_uturn_progress[i] = false;
				            bad_uturn_state[i] = -1;

				            this->vehicle.moveToXY(bad_uturn_list[i], "", 0, (this->vehicle.getPosition(bad_uturn_list[i]).x) , (this->vehicle.getPosition(bad_uturn_list[i]).y), this->vehicle.getAngle(bad_uturn_list[i]) , 1);
		//                    this->vehicle.setRoute(bad_uturn_list[i], new_route);
				            this->vehicle.rerouteTraveltime(bad_uturn_list[i]);

				            bad_uturn_list.erase(bad_uturn_list.begin() + i);
				            bad_uturn_on_lane.erase(bad_uturn_on_lane.begin() + i);
				            bad_uturn_at_pos.erase(bad_uturn_at_pos.begin() + i);
				            bad_uturn_veh_atlane_now.erase(bad_uturn_veh_atlane_now.begin() + i);
				            bad_uturn_state.erase(bad_uturn_state.begin() + i);
				            bad_uturn_progress.erase(bad_uturn_progress.begin() + i);
				        }
					}
				}

            }
            /**speeding return to normal*/
            for(int i = 0; i<speed_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), speed_list[i]);
                if(it != list.end() && odd_event_return_perc <= speeding_slow_perc){
                    odd_event_return_perc = distd(genu);
                    this->vehicle.setSpeed(speed_list[i], -1);
                    speed_list.erase(speed_list.begin() + i);
                    speed_list_speed.erase(speed_list_speed.begin() + i);
                }
            }
            /**speeding algorithm*/
            for(int i = 0; i<speed_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), speed_list[i]);
                if(it != list.end()){
                    double new_speed = this->vehicle.getSpeed(speed_list[i]) + speed_list_speed[i];
                    /** || new_speed > this->lane.getMaxSpeed(this->vehicle.getLaneID(speed_list[i]))*/
                    if(new_speed > this->vehicle.getMaxSpeed(speed_list[i]) || new_speed > this->lane.getMaxSpeed(this->vehicle.getLaneID(speed_list[i]))){
                        this->vehicle.setMaxSpeed(speed_list[i], new_speed);
                        this->lane.setMaxSpeed(this->vehicle.getLaneID(speed_list[i]), new_speed);
                    }
                    this->vehicle.setSpeed(speed_list[i], new_speed);
                    if(this->print_file){
                        label_one(this->file_label, "speed", speed_list[i]);
                    }
                    if(this->print_file_instance){
                        label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "speed", speed_list[i]);
                    }
                }
                else{
                    std::vector<std::string>::iterator bit = std::find(speed_list.begin(), speed_list.end(), speed_list[i]);
                    int bit_at = bit - speed_list.begin();
                    speed_list_speed.erase(speed_list_speed.begin() + bit_at);
                    speed_list.erase(std::remove(speed_list.begin(), speed_list.end(), speed_list[i]), speed_list.end());
                }
            }
            /**slow return to normal*/
            for(int i = 0; i<slow_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), slow_list[i]);
                if(it != list.end() && odd_event_return_perc <= speeding_slow_perc){
                    odd_event_return_perc = distd(genu);
                    this->vehicle.setSpeed(slow_list[i], -1);
                    slow_list.erase(slow_list.begin() + i);
                    slow_list_slow.erase(slow_list_slow.begin() + i);
                }
            }
            /**slow algorithm*/
            for(int i = 0; i<slow_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), slow_list[i]);
                if(it != list.end()){
                    double new_speed = this->vehicle.getSpeed(slow_list[i]) - slow_list_slow[i];
                    /** || new_speed > this->lane.getMaxSpeed(this->vehicle.getLaneID(slow_list[i]))*/
                    if(new_speed > this->vehicle.getMaxSpeed(slow_list[i])){
                        this->vehicle.setMaxSpeed(slow_list[i], new_speed);
                        this->lane.setMaxSpeed(this->vehicle.getLaneID(slow_list[i]), new_speed);
                    }
                    if(new_speed < 0){
                        new_speed = distd(genu) * 2 + 2;
                    }
                    this->vehicle.setSpeed(slow_list[i], new_speed);
                    if(this->print_file){
                        label_one(this->file_label, "slow", slow_list[i]);
                    }
                    if(this->print_file_instance){
                        label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "slow", slow_list[i]);
                    }
                }
                else{
                    std::vector<std::string>::iterator bit = std::find(slow_list.begin(), slow_list.end(), slow_list[i]);
                    int bit_at = bit - slow_list.begin();
                    slow_list_slow.erase(slow_list_slow.begin() + bit_at);
                    slow_list.erase(std::remove(slow_list.begin(), slow_list.end(), slow_list[i]), slow_list.end());
                }
            }
            /**speed slow return to normal*/
            for(int i = 0; i<speed_slow_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), speed_slow_list[i]);
                if(it != list.end() && odd_event_return_perc <= speeding_slow_perc){
                    odd_event_return_perc = distd(genu);
                    this->vehicle.setSpeed(speed_slow_list[i], -1);
                    speed_slow_list.erase(speed_slow_list.begin() + i);
                }
            }
            /**fast slow algorithm*/
            for(int i = 0; i<speed_slow_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), speed_slow_list[i]);
                if(it != list.end()){
                    double change_speed = distb(genu) * 13;
                    double new_speed = change_speed;
                    /** || new_speed > this->lane.getMaxSpeed(this->vehicle.getLaneID(speed_slow_list[i]))*/
                    if(new_speed > this->vehicle.getMaxSpeed(speed_slow_list[i])){
                        this->vehicle.setMaxSpeed(speed_slow_list[i], new_speed);
                        this->lane.setMaxSpeed(this->vehicle.getLaneID(speed_slow_list[i]), new_speed);
                    }
                    if(new_speed < 0){
//                        new_speed = distd(genu) * 2 + 2;
                        new_speed = 0;
                    }
                    if(c % 10){
                        this->vehicle.setSpeed(speed_slow_list[i], new_speed);

                        if(this->print_file){
                            label_one(this->file_label, "spsl", speed_slow_list[i]);
                        }
                        if(this->print_file_instance){
                            label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "spsl", speed_slow_list[i]);
                        }
                    }
                }
                else{
                    speed_slow_list.erase(std::remove(speed_slow_list.begin(), speed_slow_list.end(), speed_slow_list[i]), speed_slow_list.end());
                }
            }
            /**off road straight algorithm*/
            for(int i = 0; i<off_str_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), off_str_list[i]);
                if(it != off_str_list.end() && (this->vehicle.getLaneIndex(off_str_list[i]) == 0 || off_str_progress[i])){
                    double veh_pos_now = this->vehicle.getLanePosition(off_str_list[i]);
                    if(!off_str_progress[i]){
                        if(this->lane.getLength(this->vehicle.getLaneID(off_str_list[i])) * off_road_pos_perc >= (veh_pos_now - this->lane.getLength(this->vehicle.getLaneID(off_str_list[i])) * 0.05) && this->lane.getLength(this->vehicle.getLaneID(off_str_list[i])) * off_road_pos_perc <= (veh_pos_now + this->lane.getLength(this->vehicle.getLaneID(off_str_list[i])) * 0.05)){
                            off_str_progress[i] = true;
                        }
                    }
                    if(off_str_progress[i] == true && this->vehicle.getLaneIndex(off_str_list[i]) == 0){

                        double off_angle = this->vehicle.getAngle(off_str_list[i]);
                        if(off_str_state[i] == 0)
                            off_angle += 20;
                        if(off_str_state[i] == 5)
                            off_angle -= 20;
                        if(off_str_state[i] == off_str_duration - 5)
                            off_angle -= 20;
                        if(off_str_state[i] == off_str_duration)
                            off_angle += 20;

                        double turn_x = this->vehicle.getSpeed(off_str_list[i]) / 20 * cos((off_angle - 90) / 180 * M_PI);
                        double turn_y = this->vehicle.getSpeed(off_str_list[i]) / 20 * sin((off_angle + 90) / 180 * M_PI);
                        this->vehicle.setSpeed(off_str_list[i], this->vehicle.getSpeed(off_str_list[i]));
                        /**off_str_state[i] < off_str_duration*/

                        if(this->vehicle.getLaneIndex(off_str_list[i]) == -1 || off_str_state[i] < off_str_duration){
                            this->vehicle.moveToXY(off_str_list[i], "", 0, (this->vehicle.getPosition(off_str_list[i]).x + turn_x) , (this->vehicle.getPosition(off_str_list[i]).y + turn_y), off_angle , 2);
                            if(this->print_file){
                                label_one(this->file_label, "offs", off_str_list[i]);
                            }
                            if(this->print_file_instance){
                                label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "offs", off_str_list[i]);
                            }
                        }
                        /**off_str_state[i] == off_str_duration*/
                        else if(this->vehicle.getLaneIndex(off_str_list[i]) != -1){
//                            this->vehicle.moveToXY(off_str_list[i], "", 0, (this->vehicle.getPosition(off_str_list[i]).x) , (this->vehicle.getPosition(off_str_list[i]).y), this->vehicle.getAngle(off_str_list[i]) , 1);
//                            this->vehicle.setRoute(off_str_list[i], new_route);
                            this->vehicle.rerouteTraveltime(off_str_list[i]);

                            std::vector<std::string>::iterator oit = std::find(off_str_list.begin(), off_str_list.end(), off_str_list[i]);
                            int off_at = oit - off_str_list.begin();
                            off_str_list.erase(off_str_list.begin() + off_at);
                            off_str_progress.erase(off_str_progress.begin() + off_at);
                            off_str_state.erase(off_str_state.begin() + off_at);

                        }

                        off_str_state[i]++;
                    }
                    else if(this->vehicle.getLaneIndex(off_str_list[i]) != 0){
                        std::vector<std::string>::iterator oit = std::find(off_str_list.begin(), off_str_list.end(), off_str_list[i]);
                        int off_at = oit - off_str_list.begin();
                        off_str_list.erase(off_str_list.begin() + off_at);
                        off_str_progress.erase(off_str_progress.begin() + off_at);
                        off_str_state.erase(off_str_state.begin() + off_at);
                    }

                }
                else{
                    std::vector<std::string>::iterator oit = std::find(off_str_list.begin(), off_str_list.end(), off_str_list[i]);
                    int off_at = oit - off_str_list.begin();
                    off_str_list.erase(off_str_list.begin() + off_at);
                    off_str_progress.erase(off_str_progress.begin() + off_at);
                    off_str_state.erase(off_str_state.begin() + off_at);
                }
            }
            /**off road per algorithm*/
            for(int i = 0; i<off_per_list.size(); i++){
                std::vector<std::string> list = this->vehicle.getIDList();
                std::vector<std::string>::iterator it = std::find(list.begin(), list.end(), off_per_list[i]);
                if(it != off_per_list.end() && (this->vehicle.getLaneIndex(off_per_list[i]) == 0 || off_per_progress[i])){
                    double veh_pos_now = this->vehicle.getLanePosition(off_per_list[i]);
                    if(!off_per_progress[i]){
                        if(this->lane.getLength(this->vehicle.getLaneID(off_per_list[i])) * off_per_pos_perc >= (veh_pos_now - this->lane.getLength(this->vehicle.getLaneID(off_per_list[i])) * 0.05) && this->lane.getLength(this->vehicle.getLaneID(off_per_list[i])) * off_per_pos_perc <= (veh_pos_now + this->lane.getLength(this->vehicle.getLaneID(off_per_list[i])) * 0.05)){
                            off_per_progress[i] = true;
    //                        std::cout << "HERE" << std::endl;
                        }
                    }

                    if(off_per_progress[i] == true && (this->vehicle.getLaneIndex(off_per_list[i]) == 0 || this->vehicle.getLaneIndex(off_per_list[i]) == -1) ){
                        double off_angle = this->vehicle.getAngle(off_per_list[i]);
                        double d_angle = 90 / off_per_duration;
                        double to_change_angle = distd(genu) * 10 - 5;
//                        off_angle += d_angle;
                        off_angle += to_change_angle;

                        double turn_x = (this->vehicle.getSpeed(off_per_list[i]) + 5) / 15 * cos((off_angle - 90) / 180 * M_PI);
                        double turn_y = (this->vehicle.getSpeed(off_per_list[i]) + 5) / 15 * sin((off_angle + 90) / 180 * M_PI);
                        this->vehicle.setSpeed(off_per_list[i], this->vehicle.getSpeed(off_per_list[i]));

//                        this->vehicle.moveToXY(off_per_list[i], eID, 0, (this->vehicle.getPosition(off_per_list[i]).x + turn_x) , (this->vehicle.getPosition(off_per_list[i]).y + turn_y), off_angle , 2);
                        /**off_str_state[i] < off_str_duration*/
//                        if(off_per_state[i] == 0){
                            this->vehicle.moveToXY(off_per_list[i], "", 0, (this->vehicle.getPosition(off_per_list[i]).x + turn_x) , (this->vehicle.getPosition(off_per_list[i]).y + turn_y), off_angle , 2);
                            off_per_state[i]++;
                            if(this->print_file){
                                label_one(this->file_label, "offp", off_per_list[i]);
                            }
                            if(this->print_file_instance){
                                label_instance(this->file_label_instance, this->simulation.getCurrentTime(), "offp", off_per_list[i]);
                            }
//                        }
//                        else if(off_per_state[i] != 0 && this->vehicle.getLaneIndex(off_per_list[i]) == -1){
//                            this->vehicle.moveToXY(off_per_list[i], "", 0, (this->vehicle.getPosition(off_per_list[i]).x + turn_x) , (this->vehicle.getPosition(off_per_list[i]).y + turn_y), off_angle , 2);
//                            off_per_state[i]++;
//                        }
                        /**off_str_state[i] == off_str_duration*/
                        if(this->vehicle.getLaneIndex(off_per_list[i]) != -1 && off_per_state[i] > 50){
                            this->vehicle.moveToXY(off_per_list[i], "", 0, (this->vehicle.getPosition(off_per_list[i]).x) , (this->vehicle.getPosition(off_per_list[i]).y), this->vehicle.getAngle(off_per_list[i]) , 1);
//                            this->vehicle.setRoute(off_per_list[i], new_route);
                            this->vehicle.rerouteTraveltime(off_per_list[i]);

//                            std::cout << "HERE\t aaaa" << this->vehicle.getSpeed(off_per_list[i]) << std::endl;
                            std::vector<std::string>::iterator oit = std::find(off_per_list.begin(), off_per_list.end(), off_per_list[i]);
                            int off_at = oit - off_per_list.begin();
                            off_per_list.erase(off_per_list.begin() + off_at);
                            off_per_progress.erase(off_per_progress.begin() + off_at);
                            off_per_state.erase(off_per_state.begin() + off_at);

                        }
                        else if(off_per_state[i] >= off_per_duration){
                            this->vehicle.remove(off_per_list[i]);
                            std::vector<std::string>::iterator oit = std::find(off_per_list.begin(), off_per_list.end(), off_per_list[i]);
                            int off_at = oit - off_per_list.begin();
                            off_per_list.erase(off_per_list.begin() + off_at);
                            off_per_progress.erase(off_per_progress.begin() + off_at);
                            off_per_state.erase(off_per_state.begin() + off_at);

                        }

//                        off_per_state[i]++;
                    }
                    else if(this->vehicle.getLaneIndex(off_per_list[i]) != 0){
                        std::vector<std::string>::iterator oit = std::find(off_per_list.begin(), off_per_list.end(), off_per_list[i]);
                        int off_at = oit - off_per_list.begin();
                        off_per_list.erase(off_per_list.begin() + off_at);
                        off_per_progress.erase(off_per_progress.begin() + off_at);
                        off_per_state.erase(off_per_state.begin() + off_at);
                    }

                }
                else{
                    std::vector<std::string>::iterator oit = std::find(off_per_list.begin(), off_per_list.end(), off_per_list[i]);
                    int off_at = oit - off_per_list.begin();
                    off_per_list.erase(off_per_list.begin() + off_at);
                    off_per_progress.erase(off_per_progress.begin() + off_at);
                    off_per_state.erase(off_per_state.begin() + off_at);
                }
            }

            /**fixing speed zone, speeding stay speeding, other limited*/
            control_vehicle_bytime_byzone(v_list, speed_list);

            /**handling stuck vehicles*/
            for(int i = 0; i< v_list.size(); i++){
                /** check for existing stuck*/
                int stuck_at = check_exist(this->stuck_veh, v_list[i]);
                if(stuck_at >= 0){
                    if(this->vehicle.getSpeed(stuck_veh[stuck_at]) == 0 && this->stuck_count[stuck_at] <= this->sc){
                        this->stuck_count[stuck_at]++;
                    }
                    else if(this->vehicle.getSpeed(stuck_veh[stuck_at]) != 0 && this->stuck_count[stuck_at] <= this->sc){
                        this->stuck_veh.erase(this->stuck_veh.begin() + stuck_at);
                        this->stuck_count.erase(this->stuck_count.begin() + stuck_at);
                    }
                    else if(this->vehicle.getSpeed(stuck_veh[stuck_at]) == 0 && this->stuck_count[stuck_at] > this->sc && this->stuck_veh[stuck_at] != vID){
                        std::cout << "Removing STUCK Vehicle: " << this->stuck_veh[stuck_at] << std::endl;
                        this->vehicle.remove(this->stuck_veh[stuck_at]);
                        this->stuck_veh.erase(this->stuck_veh.begin() + stuck_at);
                        this->stuck_count.erase(this->stuck_count.begin() + stuck_at);
                    }
                }
                else if(stuck_at == -1){
                    /**excluding these group anomalies*/
                    bool addToStuckList = true;
                    for(auto g : this->coord_id){
                        for(auto gg : g){
                            if(gg == v_list[i]){
                                addToStuckList = false;
                            }
                        }
                    }
                    for(auto g : this->drag_id){
                        if(g == v_list[i]){
                            addToStuckList = false;
                        }
                    }
                    for(auto g : group_army_id){
                        for(auto gg : g){
                            if(gg == v_list[i]){
                                addToStuckList = false;
                            }
                        }
                    }
                    if(this->vehicle.getSpeed(v_list[i]) == 0 && addToStuckList){
                        this->stuck_veh.push_back(v_list[i]);
                        this->stuck_count.push_back(1);
                    }
                }
            }
            c++;
        }
        log_time.timeEnd();
        std::cout << "Quitting SUMO. Simulated Time in ms: " << this->simulation.getCurrentTime() << std::endl;
        this->close();
        stop = true;
    }

    void contsim (){
        initscr();
        crmode();
        keypad(stdscr, TRUE);

        this->key = getch();
        while(!this->stop){
            //this->x=0; this->da=0;
            //std::cout << (int)this->key << std::endl;
            if(this->user_vehicle){
                switch(this->key){
                    case KEY_RIGHT: this->da += 15;     std::cout << "Right" << std::endl; break;
                    case KEY_LEFT:  this->da -= 15;     std::cout << "Left" << std::endl; break;
                    case KEY_UP:    this->speed += 0.1; std::cout << "Speed Up" << std::endl; break;
                    case KEY_DOWN:  this->speed -= 0.1; std::cout << "Slow Down" << std::endl; break;
    				/**esc*/
                    case 27:	    this->esc = true;   std::cout << "Exiting" << std::endl; break;
    				/**s*/
                    case 115:       this->speed = 0;    std::cout << "Stop" << std::endl; break;
    				/**a*/
                    case 97:        this->speed += 2;   std::cout << "Fast Acceleration" << std::endl; break;
    				/**d*/
    				case 100:	    this->del = true; std::cout << "Deleting Vehicle" << std::endl; break;
    				/**f*/
    				case 102:	    this->add = true; std::cout << "Adding Vehicle" << std::endl; break;
    				/**r*/
    				case 114:	    this->uturn_b = true; std::cout << "Add U-turn Vehicle" << std::endl; break;
                }
                if (this->speed < 0){
                    this->speed = 0;
                }
                if(this->speed > this->mv && !this->del){
                    this->vehicle.setMaxSpeed(this->strv, this->speed);
                }
            }
            else{
                switch(this->key){
                    case 27:	    this->esc = true;   std::cout << "Exiting" << std::endl; break;
                }
            }
            this->key = getch();
        }
        endwin();
        exit(EXIT_SUCCESS);
    };

    std::string::size_type sz;
    std::string file_label, file_label_instance, file_label_user, file_time_log;
    bool print_file, print_file_instance, print_file_user;
    std::vector<speed_area> defined_limit_area, define_anom_area;
    speed_area temp_area, temp_area_anom;
    normal_simulation_parameters norm_sim_para;
    anomaly_simulation_parameters anom_sim_para;
    std::string simulation_mode, simulation_increment;
    double simulation_density;
    double vehicle_type_count = 0;
    double increase_anom_prob;
    double route_check_time;

    void load_settings(const char * file){
    	pugi::xml_document doc;
		pugi::xml_parse_result result = doc.load_file(file);

		pugi::xml_node root;
		root = doc.child("setting");

		for(pugi::xml_node parameter = root.first_child(); parameter; parameter = parameter.next_sibling()){
			// std::cout << parameter.name() << std::endl;
			for(pugi::xml_attribute big_attr = parameter.first_attribute(); big_attr; big_attr = big_attr.next_attribute()){
				// std::cout << big_attr.name() << std::endl;
				if(std::string(parameter.name()) == "normal_vehicle" && std::string(big_attr.name()) == "uturn"){
					this->norm_sim_para.normal_uturn_rate = std::stod(big_attr.value(), &sz);
				}
                else if(std::string(parameter.name()) == "global" && std::string(big_attr.name()) == "route_check_time"){
					this->route_check_time = std::stod(big_attr.value(), &sz);
				}
                else if(std::string(parameter.name()) == "global" && std::string(big_attr.name()) == "time_log"){
					this->file_time_log = std::string(big_attr.value());
				}
                else if(std::string(parameter.name()) == "anom_event" && std::string(big_attr.name()) == "prob"){
					this->anom_sim_para.anomaly_event_probability = std::stod(big_attr.value(), &sz);
				}
				else if(std::string(parameter.name()) == "user" && std::string(big_attr.name()) == "add"){
                    std::string input = std::string(big_attr.value());
                    if(input == "true" || input == "True" || input == "t" || input == "T" ||
                        input == "yes" || input == "Yes" || input == "Y" || input == "y"){
                            this->user_vehicle = true;
                        }
                    else if(input == "false" || input == "False" || input == "f" || input == "F" ||
                        input == "no" || input == "No" || input == "N" || input == "n"){
                        this->user_vehicle = false;
                    }
                    else{
                        std::cout << "Error: Input adding user vehicle is not valid" << std::endl;
                    }
				}
                else if(std::string(parameter.name()) == "anom_event" && std::string(big_attr.name()) == "group"){
                    this->anom_sim_para.anomaly_group_event_probability = std::stod(big_attr.value(), &sz) * this->anom_sim_para.anomaly_event_probability;
                }
				else if(std::string(parameter.name()) == "simulation" && std::string(big_attr.name()) == "mode"){
					this->simulation_mode = std::string(big_attr.value());
				}
				else if(std::string(parameter.name()) == "simulation" && std::string(big_attr.name()) == "increment"){
					this->simulation_increment = std::string(big_attr.value());
				}
				else if(std::string(parameter.name()) == "simulation" && std::string(big_attr.name()) == "density"){
					this->simulation_density = std::stod(big_attr.value(), &sz);
				}
			}
			for(pugi::xml_node cat = parameter.first_child(); cat; cat = cat.next_sibling()){
				// std::cout << "\t" << cat.name() << std::endl;

				if(std::string(parameter.name()) == "normal_vehicle"){
					this->vehicle_type_count++;
				}
				for(pugi::xml_attribute attr = cat.first_attribute(); attr; attr = attr.next_attribute()){
					//std::cout << attr.name() << std::endl;
					if(std::string(parameter.name()) == "label"){
						if(std::string(cat.name()) == "anom" && std::string(attr.name()) == "file"){
							this->file_label = std::string(attr.value());
						}
						else if(std::string(cat.name()) == "instance" && std::string(attr.name()) == "file"){
							this->file_label_instance = std::string(attr.value());
						}
                        else if(std::string(cat.name()) == "user" && std::string(attr.name()) == "file"){
							this->file_label_user = std::string(attr.value());
						}
                        else if(std::string(cat.name()) == "anom" && std::string(attr.name()) == "print"){
                            if(std::string(attr.value()) == "true" || std::string(attr.value()) == "True" || std::string(attr.value()) == "t" || std::string(attr.value()) == "T" ||
                                std::string(attr.value()) == "yes" || std::string(attr.value()) == "Yes" || std::string(attr.value()) == "y" || std::string(attr.value()) == "Y"){
                                this->print_file = true;
                            }
                            else if(std::string(attr.value()) == "false" || std::string(attr.value()) == "False" || std::string(attr.value()) == "f" || std::string(attr.value()) == "F" ||
                                std::string(attr.value()) == "no" || std::string(attr.value()) == "No" || std::string(attr.value()) == "n" || std::string(attr.value()) == "N"){
                                this->print_file = false;
                            }
                            else{
                                std::cout << "Error: Input for printing anomaly vehicle is not valid" << std::endl;
                            }
                        }
                        else if(std::string(cat.name()) == "instance" && std::string(attr.name()) == "print"){
                            if(std::string(attr.value()) == "true" || std::string(attr.value()) == "True" || std::string(attr.value()) == "t" || std::string(attr.value()) == "T" ||
                                std::string(attr.value()) == "yes" || std::string(attr.value()) == "Yes" || std::string(attr.value()) == "y" || std::string(attr.value()) == "Y"){
                                this->print_file_instance = true;
                            }
                            else if(std::string(attr.value()) == "false" || std::string(attr.value()) == "False" || std::string(attr.value()) == "f" || std::string(attr.value()) == "F" ||
                                std::string(attr.value()) == "no" || std::string(attr.value()) == "No" || std::string(attr.value()) == "n" || std::string(attr.value()) == "N"){
                                this->print_file_instance = false;
                            }
                            else{
                                std::cout << "Error: Input for printing anomaly vehicle instance is not valid" << std::endl;
                            }
                        }
                        else if(std::string(cat.name()) == "user" && std::string(attr.name()) == "print"){
                            if(std::string(attr.value()) == "true" || std::string(attr.value()) == "True" || std::string(attr.value()) == "t" || std::string(attr.value()) == "T" ||
                                std::string(attr.value()) == "yes" || std::string(attr.value()) == "Yes" || std::string(attr.value()) == "y" || std::string(attr.value()) == "Y"){
                                this->print_file_user = true;
                            }
                            else if(std::string(attr.value()) == "false" || std::string(attr.value()) == "False" || std::string(attr.value()) == "f" || std::string(attr.value()) == "F" ||
                                std::string(attr.value()) == "no" || std::string(attr.value()) == "No" || std::string(attr.value()) == "n" || std::string(attr.value()) == "N"){
                                this->print_file_user = false;
                            }
                            else{
                                std::cout << "Error: Input for printing user vehicle is not valid" << std::endl;
                            }
                        }

					}
					else if(std::string(parameter.name()) == "zone"){
						if(std::string(cat.name()) == "area"){
							if(std::string(attr.name()) == "id"){
								this->temp_area.id = std::stoi(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "type"){
								this->temp_area.type = std::string(attr.value());
							}
							else if(std::string(attr.name()) == "sp"){
								this->temp_area.sp = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "x"){
								this->temp_area.x = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "y"){
								this->temp_area.y = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "xlen"){
								this->temp_area.xlen = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "ylen"){
								this->temp_area.ylen = std::stod(attr.value(), &sz);
							}

						}
						if(this->temp_area.id > 0 && this->temp_area.type != "" && this->temp_area.sp >= 0 && this->temp_area.x != 0 && this->temp_area.y != 0 && this->temp_area.xlen > 0 && this->temp_area.ylen > 0){
							this->defined_limit_area.push_back(this->temp_area);
							// std::cout << this->temp_area.ylen << std::endl;
							this->temp_area = {};
						}

					}
                    else if(std::string(parameter.name()) == "high_anom"){
                        if(std::string(cat.name()) == "area"){
                            if(std::string(attr.name()) == "id"){
                                this->temp_area_anom.id = std::stoi(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "type"){
                                this->temp_area_anom.type = std::string(attr.value());
                            }
                            else if(std::string(attr.name()) == "increase"){
                                this->increase_anom_prob = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "x"){
                                this->temp_area_anom.x = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "y"){
                                this->temp_area_anom.y = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "xlen"){
                                this->temp_area_anom.xlen = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "ylen"){
                                this->temp_area_anom.ylen = std::stod(attr.value(), &sz);
                            }

                        }
                        if(this->temp_area_anom.id > 0 && this->temp_area_anom.type != "" && this->temp_area_anom.sp >= 0 && this->temp_area_anom.x != 0 && this->temp_area_anom.y != 0 && this->temp_area_anom.xlen > 0 && this->temp_area_anom.ylen > 0){
                            this->define_anom_area.push_back(this->temp_area_anom);
                            // std::cout << this->temp_area_anom.ylen << std::endl;
                            this->temp_area_anom = {};
                        }

                    }
					else if(std::string(parameter.name()) == "normal_vehicle"){

						if(std::string(cat.name()) == "non"){
							this->norm_sim_para.non = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "car"){
							this->norm_sim_para.car = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "truck"){
							this->norm_sim_para.truck = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "bus"){
							this->norm_sim_para.bus = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "semi-trailer"){
							this->norm_sim_para.semitrailer = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "van"){
							this->norm_sim_para.van = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "moto"){
							this->norm_sim_para.moto = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "truck-trailer"){
							this->norm_sim_para.trucktrailer = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "car-trailer"){
							this->norm_sim_para.cartrailer = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "authority"){
							this->norm_sim_para.authority = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "emergency"){
							this->norm_sim_para.emergency = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "army"){
							if(std::string(attr.name()) == "prob"){
								this->norm_sim_para.army = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "min"){
								this->norm_sim_para.army_min = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "max"){
								this->norm_sim_para.army_max = std::stod(attr.value(), &sz);
							}

						}

					}
					else if(std::string(parameter.name()) == "anom_event"){
						if(std::string(cat.name()) == "lane"){
							this->anom_sim_para.anomaly_lane = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "uturn"){
							this->anom_sim_para.anomaly_uturn = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "off"){
							this->anom_sim_para.anomaly_off = std::stod(attr.value(), &sz);
						}
						else if(std::string(cat.name()) == "speed"){
							if(std::string(attr.name()) == "prob"){
								this->anom_sim_para.anomaly_speed = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "sp"){
								this->anom_sim_para.anomaly_speed_sp = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "slow"){
								this->anom_sim_para.anomaly_speed_slow = std::stod(attr.value(), &sz);
							}
							else if(std::string(attr.name()) == "spsl"){
								this->anom_sim_para.anomaly_speed_spsl = std::stod(attr.value(), &sz);
							}
						}
                        else if(std::string(cat.name()) == "drag"){
                            if(std::string(attr.name()) == "prob"){
                                this->anom_sim_para.anomaly_drag = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "startx"){
                                this->anom_sim_para.anomaly_drag_startx = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "starty"){
                                this->anom_sim_para.anomaly_drag_starty = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "endx"){
                                this->anom_sim_para.anomaly_drag_endx = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "endy"){
                                this->anom_sim_para.anomaly_drag_endy = std::stod(attr.value(), &sz);
                            }
                        }
                        else if(std::string(cat.name()) == "parallel_army"){
                            if(std::string(attr.name()) == "prob"){
                                this->anom_sim_para.anomaly_parallel_army = std::stod(attr.value(), &sz);
                            }
                        }
                        else if(std::string(cat.name()) == "coord_motion"){
                            if(std::string(attr.name()) == "prob"){
                                this->anom_sim_para.anomaly_coord_motion = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "duration_frame"){
                                this->anom_sim_para.group_wait_time = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "min"){
                                this->anom_sim_para.coord_min = std::stod(attr.value(), &sz);
                            }
                            else if(std::string(attr.name()) == "max"){
                                this->anom_sim_para.coord_max = std::stod(attr.value(), &sz);
                            }
                        }
					}
				}
			}
		}
    }


    Client(){
    	this->load_settings("setting.xml");
    	if(this->file_label == "" || this->file_label_instance == "" || this->file_label_user == ""){
    		std::cout << "Error: File name for logging labels" << std::endl;
    		exit(EXIT_SUCCESS);
    	}
    	if(this->simulation_mode == ""){
    		std::cout << "Error: Simulation mode not defined" << std::endl;
    		exit(EXIT_SUCCESS);
    	}



        this->duration_count = 0;
        this->duration = 1000;

        this->sc = 1000;

		this->uturn_b = false;
		this->uturn_e = false;
		this->uturn_d = false;
		this->uturn_prog = false;
		this->uturn_speed = {5.7557, 6.0637, 6.5637, 7.0637, 7.5463, 8.0251, 8.4489, 8.7252, 9.0401, 9.3443, 9.6911, 10.1875, 10.5130};
		this->uturn_sp_diff = {0.3080, 0.5000, 0.5000, 0.4826, 0.4789, 0.4237, 0.2763, 0.3149, 0.3043, 0.3467, 0.4964, 0.3255, 0.3322};
		this->uturn_angle_diff = {-3.7811,-4.8574,-7.7386, -12.3158, -20.4852, -28.7145, -29.1845, -29.3436, -21.1367, -12.4976, -7.5941, -2.3510, 0};
        this->flag = false;
        this->stop = false;
        this->esc = false;
		this->del = false;
		this->add = false;
		this->adding = false;
		this->added = true;
		this->r_id = 0;
        this->strr = "1";
        this->stre = "";
        this->y = 0;
        this->x = 0;
        this->speed = 0;
        this->da = 0;
        this->p_id = 0;
        this->dp_id = 0;

        this->customRouteNumber = 0;

        this->connect("localhost", 1337);
        //std::cout << "time in ms: " << this->simulation.getCurrentTime() << std::endl;
        //std::cout << "run 5 steps ... " << std::endl;
        std::cout << "Connected to SUMO" << std::endl;
    };
};

int main(int argc, char* argv[]){
    Client client;
    boost::thread th1(&Client::loopsim, &client);
    boost::thread th2(&Client::contsim, &client);
    th1.join();
    th2.join();
}
