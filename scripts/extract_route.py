import xml.etree.ElementTree
import xml.etree.ElementTree as ET
from xml.dom import minidom

rr = []

e = xml.etree.ElementTree.parse('../input/map.rou.xml').getroot()

for v in e:
	for r in v:
		#print(r.get('edges'))
		if(r.get('edges') not in rr):
			rr.append(r.get('edges'))


#print(rr, len(rr))

i = 1;
routes = ET.Element("routes")
#route = ET.SubElement(routes, "route")

for x in rr:
	ET.SubElement(routes, "route", id = str(i) , edges = x, arrivalPos="-1", departpos="-1")
	i = i + 1

xmlstr = minidom.parseString(ET.tostring(routes)).toprettyxml(indent="	")
with open("../input/out.xml", "w") as f:
    	f.write(xmlstr.encode('utf-8'))
