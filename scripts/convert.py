import os, sys, getopt

sumoRootPath = ''
filename = ''

try:
	opts, args = getopt.getopt(sys.argv[1:], 'hs:o:', ['sumo=','osm=','help='])
except getopt.GetoptError:
	print("Error: convert.py - Invalid arguments")
	sys.exit(2)
for opt, arg in opts:
	if opt == '-h':
		print('Usage: python convert.py [OPTION]')
		print('\t -s, --sumo\t\tSUMO root path')
		print('\t -o, --osm\t\toutput osm filename')
		print('\t -h, --help\t\thelp menu')
		sys.exit()
	elif opt in ('-s', '--sumo'):
		sumoRootPath = arg
	elif opt in ('-o', '--osm'):
		filename = arg
			
if filename == '':
	filename = "generated.osm"

if sumoRootPath == '':
	print('Error: need SUMO root path')
	exit(1)



os.system("netconvert --osm-files " + filename + " --output.street-names -o final.net.xml --opposites.guess.fix-lengths true")
#os.system("polyconvert --net-file final.net.xml --osm-files *.osm --type-file osmPolyconvert.typ.xml -o map.poly.xml');
os.system("python " + sumoRootPath + "/tools/randomTrips.py --min-distance 2500 -n final.net.xml -r map.rou.xml -e 36000")
os.system("python ../scripts/extract_route.py")
os.system("python ../scripts/extract_lane.py")
os.system("python ../scripts/extract_lane_detection.py")

f = open("final.sumo.cfg", "w+")
s = '<?xml version="1.0" encoding="UTF-8"?>\n\n<configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo.sf.net/xsd/sumoConfiguration.xsd">\n\t<input>\n\t\t<net-file value="final.net.xml"/>\n\t\t<!--<additional-files value="vehicles.xml,additional.xml"/> -->\n\t\t<additional-files value="vehicles.xml map.poly.xml"/>\n\t\t<route-files value="out.xml"/>\n\t</input>\n\t<time>\n\t\t<step-length value="0.1"/>\n\t</time>\n\t<output>\n\t\t<fcd-output value="fcd"/>\n\t\t<fcd-output.geo value="true"/>\n\t</output>\n\t<gui_only>\n\t\t<gui-settings-file value="view.xml"/>\n\t</gui_only>\n</configuration>'
f.write(s)
f.close()

f = open("vehicles.xml", "w+")
s = '<additional xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://sumo.sf.net/xsd/additional_file.xsd">\n\t<vType id="non_classified" accel="5.0" decel="6.0" sigma="0.5" length="4.5" minGap="1.5" maxSpeed="60" guiShape="passenger" vClass="private"/>\n\t<vType id="car" accel="6.0" decel="6.0" sigma="0.5" length="4.5" minGap="1.5" maxSpeed="60" guiShape="passenger" vClass="private"/>\n\t<vType id="car_trailer" accel="3.5" decel="5.0" sigma="0.5" length="7.5" minGap="1.5" maxSpeed="60" guiShape="passenger" vClass="private"/>\n\t<vType id="truck" accel="1.5" decel="4.0" sigma="0.5" length="12.0" minGap="1.5" maxSpeed="20" guiShape="transport" vClass="private"/>\n\t<vType id="truck_trailer" accel="1.0" decel="3.5" sigma="0.5" length="20.0" minGap="1.5" maxSpeed="10" guiShape="transport/trailer" vClass="private"/>\n\t<vType id="bus" accel="2.6" decel="4.5" sigma="0.5" length="18" minGap="3" maxSpeed="60" color="1,1,0" guiShape="bus" vClass="private"/>\n\t<vType id="semitrailer" accel="3.0" decel="3.5" sigma="0.5" length="20.0" minGap="1.5" maxSpeed="60" guiShape="transport/semitrailer" vClass="private"/>\n\t<vType id="van" accel="3.5" decel="5.5" sigma="0.5" length="5.0" minGap="1.5" maxSpeed="60" guiShape="delivery" vClass="private"/>\n\t<vType id="motorcycle" accel="6.0" decel="5.5" sigma="0.5" length="2.0" minGap="1.5" maxSpeed="220" guiShape="motorcycle" vClass="private"/>\n\t<vType id="emergency" accel="6.0" decel="6.0" sigma="0.5" length="5" minGap="1.5" maxSpeed="150" guiShape="delivery" vClass="emergency" jmDriveAfterRedTime="300" jmIgnoreFoeProb="0.8" jmIgnoreFoeSpeed="100" impatience="1"/>\n\t<vType id="authority" accel="6.0" decel="6.0" sigma="0.5" length="4.5" minGap="1.5" maxSpeed="150" guiShape="passenger" vClass="authority" jmDriveAfterRedTime="300" jmIgnoreFoeProb="0.8" jmIgnoreFoeSpeed="100" impatience="1"/>\n\t<vType id="army" accel="3.0" decel="3.0" sigma="0.5" length="5.5" minGap="3" maxSpeed="8" guiShape="delivery" vClass="army" jmDriveAfterRedTime="300" jmIgnoreFoeProb="0.8" jmIgnoreFoeSpeed="100" impatience="1"/>\n</additional>'
f.write(s)
f.close()

f = open("poly_type.xml", "w+")
s = '<polygonTypes>\n\t<polygonType id="waterway"                name="water"       color=".71,.82,.82" layer="-4"/>\n\t<polygonType id="natural"                 name="natural"     color=".55,.77,.42" layer="-4"/>\n\t<polygonType id="natural.water"           name="water"       color=".71,.82,.82" layer="-4"/>\n\t<polygonType id="natural.wetland"         name="water"       color=".71,.82,.82" layer="-4"/>\n\t<polygonType id="natural.wood"            name="forest"      color=".55,.77,.42" layer="-4"/>\n\t<polygonType id="natural.land"            name="land"        color=".98,.87,.46" layer="-4"/>\n\n\t<polygonType id="landuse"                 name="landuse"     color=".76,.76,.51" layer="-3"/>\n\t<polygonType id="landuse.forest"          name="forest"      color=".55,.77,.42" layer="-3"/>\n\t<polygonType id="landuse.park"            name="park"        color=".81,.96,.79" layer="-3"/>\n\t<polygonType id="landuse.residential"     name="residential" color=".92,.92,.89" layer="-3"/>\n\t<polygonType id="landuse.commercial"      name="commercial"  color=".82,.82,.80" layer="-3"/>\n\t<polygonType id="landuse.industrial"      name="industrial"  color=".82,.82,.80" layer="-3"/>\n\t<polygonType id="landuse.military"        name="military"    color=".60,.60,.36" layer="-3"/>\n\t<polygonType id="landuse.farm"            name="farm"        color=".95,.95,.80" layer="-3"/>\n\t<polygonType id="landuse.greenfield"      name="farm"        color=".95,.95,.80" layer="-3"/>\n\t<polygonType id="landuse.village_green"   name="farm"        color=".95,.95,.80" layer="-3"/>\n\n\t<polygonType id="tourism"                 name="tourism"     color=".81,.96,.79" layer="-2"/>\n\t<polygonType id="military"                name="military"    color=".60,.60,.36" layer="-2"/>\n\t<polygonType id="sport"                   name="sport"       color=".31,.90,.49" layer="-2"/>\n\t<polygonType id="leisure"                 name="leisure"     color=".81,.96,.79" layer="-2"/>\n\t<polygonType id="leisure.park"            name="tourism"     color=".81,.96,.79" layer="-2"/>\n\t<polygonType id="aeroway"                 name="aeroway"     color=".50,.50,.50" layer="-2"/>\n\t<polygonType id="aerialway"               name="aerialway"   color=".20,.20,.20" layer="-2"/>\n\t<polygonType id="highway.services"        name="services"    color=".93,.78,1.0" layer="-2"/>\n\n\t<polygonType id="shop"                    name="shop"        color=".93,.78,1.0" layer="-1"/>\n\t<polygonType id="historic"                name="historic"    color=".50,1.0,.50" layer="-1"/>\n\t<polygonType id="man_made"                name="man_made"    color="1.0,.90,.90" layer="-1"/>\n\t<polygonType id="man_made.pipeline"       name="pipeline"    color="1.0,.90,.90" layer="-1"/>\n\t<polygonType id="building"                name="building"    color="1.0,.90,.90" layer="-1"/>\n\t<polygonType id="amenity"                 name="amenity"     color=".93,.78,.78" layer="-1"/>\n\t<polygonType id="amenity.parking"         name="parking"     color=".72,.72,.70" layer="-1"/>\n\t<polygonType id="power"                   name="power"       color=".10,.10,.30" layer="-1" discard="true"/>\n\t<polygonType id="highway"                 name="highway"     color=".10,.10,.10" layer="-1" discard="true"/>\n\n\t<polygonType id="boundary"                name="boundary"    color="1.0,.33,.33" layer="0" fill="false" discard="true"/>\n\t<polygonType id="admin_level"             name="admin_level" color="1.0,.33,.33" layer="0" fill="false" discard="true"/>\n\t<polygonType id="place"                   name="admin_level" color="1.0,.9,.0" layer="0" fill="false" discard="true"/>\n</polygonTypes>'
f.write(s)
f.close()

f = open("view.xml", "w+")
s = '<viewsettings>\n\t<scheme name="real world"/>\n\t<delay value="20"/>\n</viewsettings>'
f.write(s)
f.close()

os.system("polyconvert --net-file final.net.xml --osm-files " + filename + " --type-file poly_type.xml -o map.poly.xml")
