# Generate SUMO useable config files from OSM
## Steps
1. Save the .osm file in this folder, make sure there is only ONE .osm file.
2. `python convert.py`

## Explanation 
1. Using build-in SUMO function netconvert to generate net file from osm, which define the roads and buildings.
2. Generate trips from SUMO toolkit `randomTrips.py`, which have vehicles and their routes they will be taking. Since TraCI requires routes to have its id associate with them, and this function does not do that. We are only using this function to generate all possible combination of routes.
3. Extracting routes and assign id to them with `extract_route.py`
4. Extracting list of roads and their number of lanes with `extract_lane.py`
5. Generate SUMO config file, which defines files we use in SUMO simulation.
6. Generate SUMO vehicle type file, which defines possible vehicle types and their behaviour.
7. Generate polygon type file, which defines the looks of various terrains and buildings. (Optional)
8. Generate default SUMO simulation parameters including viewing scheme defined in Step 7 and time delay.
9. Generate polygon file which use polygon type file in Step 7 to define what gui looks like.