import xml.etree.ElementTree
import xml.etree.ElementTree as ET
from xml.dom import minidom

def remove_none(list):
	return [e for e in list if e != None]

lane_name = []
lane_num = []
now = ""
past = ""
num = 0

too_small = 0

#was 30 for detection
threshold = 0;

ee = xml.etree.ElementTree.parse('../input/final.net.xml').getroot()

for e in ee.findall('edge'):
	if(e.get('id') not in lane_name):
		lane_name.append(e.get('id'))
	k = 0
	for l in e:
		k = k + 1
		if(float(l.get('length')) <= threshold):
			too_small = 1
		if(float(l.get('length')) > threshold):
			too_small = 0

	if(too_small == 0):
		lane_num.append(k)
	if(too_small == 1):
		lane_name.pop()



f = open("../input/lane.meas", "w")
i = 1;
#lanes = ET.Element("lanes")

for x in lane_name:
	to_write = str(x) + "," + str(lane_num[i - 1])
	f.write("%s\n"% to_write)
	#ET.SubElement(lanes, "lane", id = x, num = str(lane_num[i - 1]))
	i = i + 1
