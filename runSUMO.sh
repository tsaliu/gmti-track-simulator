#!/bin/bash
set -e

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -g|--gui)
    cd input
    gnome-terminal -x sumo-gui -c final.sumo.cfg --remote-port 1337

    shift # past argument
    ;;
    -n|--nogui)
    DEFAULT=YES
    cd input
    gnome-terminal -x sumo -c final.sumo.cfg --remote-port 1337
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters
if [[ -n $1 ]]; then
    echo "Invalid argument format"
    tail -1 "$1"
fi

#cd ../convert_osm
#gnome-terminal -x sumo-gui -c final.sumo.cfg --remote-port 1337
cd ../plugin
echo "compiling"
g++ -std=c++0x -fPIC -o pluginAnomaly TraCIAPITest.cpp utils/traci/libtraciclient.a foreign/tcpip/storage.o foreign/tcpip/socket.o -I . -lboost_system -lboost_thread -lncurses -lpugixml -lpthread
echo "done"
./pluginAnomaly
