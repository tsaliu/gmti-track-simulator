#!/bin/bash
set -e

while [ "$1" != "" ]; do
    if [ $1 == "-r" ] || [ $1 == "--right" ]; then
        shift
        right=$1
    fi
    if [ $1 == "-l" ] || [ $1 == "--left" ]; then
        shift
        left=$1
    fi
    if [ $1 == "-t" ] || [ $1 == "--top" ]; then
        shift
        top=$1
    fi
    if [ $1 == "-b" ] || [ $1 == "--bottom" ]; then
        shift
        bottom=$1
    fi

    if [ $1 == "-f" ] || [ $1 == "--file" ]; then
        shift
        filename=$1
    fi
    
    if [ $1 == "-s" ] || [ $1 == "--sumo" ]; then
        shift
        sumo=$1
    fi
    

    if [ $1 == "-h" ] || [ $1 == "--help" ]; then
        echo "Usage: ./genOSM.sh -t [OPTION] -b [OPTION] -l [OPTION] -r [OPTION] -f [OPTION]"
        echo "  -t, --top        Top Bounding Box (lat)"
        echo "  -b, --bottom     Bottom Bounding Box (lat)"
        echo "  -l, --left       Left Bounding Box (lon)"
        echo "  -r, --right      Right Bounding Box (lon)"
        echo "  -f, --file       Output OSM map name"
        echo "	-s, --sumo		 SUMO root path"
        echo "  -h, --help       Usage Menu"

        exit 1
    fi
    shift
done

if { [ "$right" == "" ] || [ "$left" == "" ] || [ "$top" == "" ] || [ "$bottom" == "" ] || [ "$sumo" == "" ]; }; then
    echo "Missing Arguments"
    echo "See -h, --help"

    exit 1
fi

export SUMO_HOME=~/Downloads/sumo-0_32_0

if [ "$filename" == "" ]; then
    filename="generated.osm"
fi
echo $filename
cd input
osmosis --read-pgsql host="130.113.23.34" database="osmosis" user="osmosis" password="osmosis" outPipe.0=pg --dataset-bounding-box inPipe.0=pg top=$top left=$left bottom=$bottom right=$right outPipe.0=dd --write-xml inPipe.0=dd "$filename"
python ../scripts/convert.py -o $filename -s $sumo
