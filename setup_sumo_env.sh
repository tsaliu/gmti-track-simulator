#!/bin/bash
set -e

path=$(pwd)
user=$(whoami)

# Get arguments
while [ "$1" != "" ]; do
    if [ $1 == "-r" ] || [ $1 == "--restart" ]; then
        shift
        restart=true
    fi

    if [ $1 == "-h" ] || [ $1 == "--help" ]; then
        echo "Usage: ./setup_sumo_env.sh [OPTION]"
        echo "  -r, --restart     	recompile/rebuild"
        echo "  -h, --help			help menu"

        exit 1
    fi
    shift
done

sudo apt-get install git wget cmake osmosis gnome-terminal python3-dev libfox-1.6-dev swig libgtest-dev libopenthreads-dev libxerces-c-dev libproj-dev libgl2ps-dev default-jdk libncurses5-dev libboost-thread* libgl1-mesa-dev -y

if test ! -d ~/Downloads/PROJ-6.1.0; then
	cd ~/Downloads
	wget https://github.com/OSGeo/PROJ/archive/6.1.0.tar.gz
	tar -xvf 6.1.0.tar.gz
	cd PROJ-6.1.0
	mkdir build
	cd build
	cmake ..
	make -j$(nproc) & sudo make install
else
	if [ "$restart" == true ]; then
		sudo rm -rf ~/Downloads/PROJ-6.1.0
		cd ~/Downloads
		wget https://github.com/OSGeo/PROJ/archive/6.1.0.tar.gz
		tar -xvf 6.1.0.tar.gz
		cd PROJ-6.1.0
		mkdir build
		cd build
		cmake ..
		make -j$(nproc) & sudo make install
	fi
fi

if ! grep -Fxq "#define ACCEPT_USE_OF_DEPRECATED_PROJ_API_H 1" /usr/local/include/proj_api.h; then
	sed -i '1s/^/#define ACCEPT_USE_OF_DEPRECATED_PROJ_API_H 1\n/' /usr/local/include/proj_api.h
fi

# Install GDAL
if test ! -d ~/Downloads/GDAL; then
	cd ~/Downloads
	git clone https://github.com/OSGeo/GDAL.git
	cd GDAL/gdal
	./configure
	make -j$(nproc) & sudo make install
else
	if [ "$restart" == true ]; then
		sudo rm -rf ~/Downloads/GDAL
		cd ~/Downloads
		git clone https://github.com/OSGeo/GDAL.git
		cd GDAL/gdal
		./configure
		make -j$(nproc) & sudo make install
	fi
fi

if test ! -f /usr/lib/libgdal.so.26; then
	sudo ln -s /usr/local/lib/libgdal.so.26 /usr/lib/libgdal.so.26
else
	if [ "$restart" == true ]; then
		sudo rm /usr/lib/libgdal.so.26
		sudo ln -s /usr/local/lib/libgdal.so.26 /usr/lib/libgdal.so.26
	fi
fi

if test ! -d ~/Downloads/OpenSceneGraph; then
	cd ~/Downloads
	git clone https://github.com/openscenegraph/OpenSceneGraph
	cd OpenSceneGraph
	mkdir build
	cd build
	cmake .. -DCMAKE_CXX_FLAGS=-std=c++11
	make -j$(nproc) & sudo make install
else 
	if [ "$restart" == true ]; then
		sudo rm -rf ~/Downloads/OpenSceneGraph
		cd ~/Downloads
		git clone https://github.com/openscenegraph/OpenSceneGraph
		cd OpenSceneGraph
		mkdir build
		cd build
		cmake .. -DCMAKE_CXX_FLAGS=-std=c++11
		make -j$(nproc) & sudo make install
	fi
fi

if test ! -f ~/Downloads/v0_32_0.tar.gz; then
	cd ~/Downloads
	wget https://github.com/eclipse/sumo/archive/v0_32_0.tar.gz
	tar -xvf v0_32_0.tar.gz
fi

if test ! -d ~/Downloads/sumo-0_32_0; then
	cd ~/Downloads
	wget https://github.com/eclipse/sumo/archive/v0_32_0.tar.gz
	tar -xvf v0_32_0.tar.gz
	sed -i '17,31{s/libsumo/libsumojni/}' sumo-0_32_0/src/libsumo/CMakeLists.txt
	cd sumo-0_32_0
	cd build
	cmake .. -DFOX_INCLUDE_DIR=/usr/local
	make -j$(nproc)
else
	if [ "$restart" == true ]; then
		sudo rm -rf ~/Downloads/sumo-0_32_0
		cd ~/Downloads
		wget https://github.com/eclipse/sumo/archive/v0_32_0.tar.gz
		tar -xvf v0_32_0.tar.gz
		sed -i '17,31{s/libsumo/libsumojni/}' sumo-0_32_0/src/libsumo/CMakeLists.txt
		cd sumo-0_32_0
		cd build
		cmake .. -DFOX_INCLUDE_DIR=/usr/local
		make -j$(nproc)
	fi
fi

if ! grep -Fxq "export SUMO_HOME=~/Downloads/sumo-0_32_0" ~/.profile; then
	echo -e "export SUMO_HOME=~/Downloads/sumo-0_32_0" | tee -a ~/.profile
fi
if ! grep -Fxq "export SUMO_HOME=~/Downloads/sumo-0_32_0" ~/.bashrc; then
	echo -e "export SUMO_HOME=~/Downloads/sumo-0_32_0" | tee -a ~/.bashrc
fi

sudo apt-get autoremove -y

cd "$path/plugin"

if test -d ./foreign; then
	rm -rf foreign
fi

if test -d ./libsumo; then
	rm -rf libsumo
fi

if test -d ./traci-server; then
	rm -rf traci-server
fi

if test -d ./utils; then
	rm -rf utils
fi

if test -f ./config.h; then
	rm -rf config.h
fi

cp -r ~/Downloads/sumo-0_32_0/src/foreign .
cp -r ~/Downloads/sumo-0_32_0/src/libsumo .
cp -r ~/Downloads/sumo-0_32_0/src/traci-server .
cp -r ~/Downloads/sumo-0_32_0/src/utils .

cp ~/Downloads/sumo-0_32_0/build/src/config.h .
cp ~/Downloads/sumo-0_32_0/build/src/foreign/tcpip/libforeign_tcpip.a ./foreign
cp ~/Downloads/sumo-0_32_0/build/src/foreign/tcpip/CMakeFiles/foreign_tcpip.dir/socket.cpp.o ./foreign/tcpip/socket.o
cp ~/Downloads/sumo-0_32_0/build/src/foreign/tcpip/CMakeFiles/foreign_tcpip.dir/storage.cpp.o ./foreign/tcpip/storage.o
cp ~/Downloads/sumo-0_32_0/build/src/traci-server/libtraciserver.a ./traci-server/libtraciserver.a
cp ~/Downloads/sumo-0_32_0/build/src/libsumo/liblibsumostatic.a ./libsumo/liblibsumostatic.a
cp ~/Downloads/sumo-0_32_0/build/src/utils/traci/libutils_traci.a ./utils/traci/libtraciclient.a

cd ~/Downloads/sumo-0_32_0/bin

for f in *
do 
if [[ ${f} == *"D" ]]
then 
cropFileName="${f:0:-1}"
sudo cp $f /usr/local/bin/$cropFileName
fi
done

cd ~/Downloads
sudo chmod -R 777 *

echo "Please re-loggin or reboot to finish setup"
